use actix::Actor;
use cnc_editor::notification::NotificationService;
use cnc_editor::ApplicationService;
use slint::ComponentHandle;

#[actix::main]
async fn main() -> Result<(), anyhow::Error> {
    pretty_env_logger::init();

    let component = cnc_editor::Application::new().unwrap();
    let component_handle = component.as_weak();

    let h = component_handle.clone();
    let arbiter = actix::Arbiter::new();
    let notification_service =
        NotificationService::start_in_arbiter(&arbiter.handle(), move |_| {
            NotificationService::new(h)
        });
    let arbiter = actix::Arbiter::new();
    let h = component_handle.clone();
    let _service = ApplicationService::start_in_arbiter(&arbiter.handle(), move |_| {
        ApplicationService::new(h, notification_service)
    });

    component.run().unwrap();
    Ok(())
}
