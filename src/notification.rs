use crate::{Application, NotificationType};
use actix::{Actor, Addr, AsyncContext, Context, Handler, Message, WrapFuture};
use slint::Weak;
use slint::{Model, ModelRc, SharedString, VecModel};
use std::rc::Rc;
use std::time::Duration;
use tokio::sync::mpsc::{self, Sender};
use tokio::sync::oneshot;
use xxhash_rust::xxh64::xxh64;

pub struct NotificationService {
    tx: Sender<(Option<String>, String, NotificationType)>,
}

type NotificationModel = VecModel<(SharedString, SharedString, SharedString, NotificationType)>;

impl NotificationService {
    pub fn new(handle: Weak<Application>) -> Self {
        let (tx, mut rx) = mpsc::channel::<(Option<String>, String, NotificationType)>(10);
        let h = handle.clone();
        tokio::task::spawn(async move {
            h.upgrade_in_event_loop(move |handle| {
                let model: Rc<NotificationModel> = Rc::new(VecModel::from(vec![]));
                handle.set_notifications(ModelRc::from(model));
                let model: Rc<VecModel<f32>> = Rc::new(VecModel::from(vec![]));
                handle.set_ny(ModelRc::from(model));
            })
            .unwrap();
            while let Some((title, text, ty)) = rx.recv().await {
                if let NotificationType::Critical = ty {
                    log::error!("Critical error occured: {:?}", (&title, &text));
                }
                let h = h.clone();
                tokio::spawn(async move {
                    let (tx, rx) = oneshot::channel();
                    h.upgrade_in_event_loop(move |handle| {
                        let the_model_rc = handle.get_notifications();
                        let the_model = the_model_rc
                            .as_any()
                            .downcast_ref::<NotificationModel>()
                            .expect("Invalid type of model");
                        let hash = xxh64(
                            format!("{text}{ty:?}").as_bytes(),
                            the_model.row_count() as u64,
                        );
                        let len = the_model.iter().count();
                        if len >= 10 {
                            the_model.remove(0);
                        }
                        tx.send(hash).expect("Unable to send hash via oneshot");
                        the_model.push((
                            hash.to_string().into(),
                            text.into(),
                            title.unwrap_or_default().into(),
                            ty,
                        ));
                        let the_model_rc = handle.get_ny();
                        let the_model = the_model_rc
                            .as_any()
                            .downcast_ref::<VecModel<f32>>()
                            .expect("Invalid type of model");
                        the_model.push(0.);
                    })
                    .unwrap();

                    let hash = rx.await.unwrap().to_string();
                    tokio::time::sleep(Duration::from_millis(3 * 1000)).await;

                    h.upgrade_in_event_loop(move |handle| {
                        let the_model_rc = handle.get_notifications();
                        let the_model = the_model_rc
                            .as_any()
                            .downcast_ref::<NotificationModel>()
                            .expect("Invalid type of model");
                        let index = the_model
                            .iter()
                            .enumerate()
                            .find(|(_, e)| e.0 == hash)
                            .map(|(i, _)| i);
                        if let Some(index) = index {
                            the_model.remove(index);
                            let the_model_rc = handle.get_ny();
                            let the_model = the_model_rc
                                .as_any()
                                .downcast_ref::<VecModel<f32>>()
                                .expect("Invalid type of model");
                            the_model.remove(index);
                        }
                    })
                    .unwrap();
                });
            }
        });
        Self { tx }
    }
}

impl Actor for NotificationService {
    type Context = Context<Self>;
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct DisplayNotification<O: Into<Option<String>>>(pub O, pub String, pub NotificationType);

impl<O: Into<Option<String>>> Handler<DisplayNotification<O>> for NotificationService {
    type Result = ();

    fn handle(
        &mut self,
        DisplayNotification(title, text, ty): DisplayNotification<O>,
        ctx: &mut Self::Context,
    ) {
        let tx = self.tx.clone();
        let title = title.into();
        ctx.wait(
            async move {
                tx.send((title, text, ty)).await.unwrap();
            }
            .into_actor(self),
        );
    }
}

pub trait Notify {
    type Output;

    fn ok_or_notify(self, addr: &Addr<NotificationService>) -> Self::Output;
    fn ok_or_notify_with_opts<O: Into<Option<String>>>(
        self,
        addr: &Addr<NotificationService>,
        ty: NotificationType,
        title: O,
    ) -> Self::Output;
}

impl<T, E: std::fmt::Display> Notify for Result<T, E> {
    type Output = Option<T>;

    fn ok_or_notify(self, addr: &Addr<NotificationService>) -> Self::Output {
        match self {
            Ok(t) => Some(t),
            Err(err) => {
                addr.do_send(DisplayNotification(
                    None,
                    err.to_string(),
                    NotificationType::Error,
                ));
                None
            }
        }
    }

    fn ok_or_notify_with_opts<O: Into<Option<String>>>(
        self,
        addr: &Addr<NotificationService>,
        ty: NotificationType,
        title: O,
    ) -> Self::Output {
        match self {
            Ok(t) => Some(t),
            Err(err) => {
                addr.do_send(DisplayNotification(title.into(), err.to_string(), ty));
                None
            }
        }
    }
}
