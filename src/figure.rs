use serde::{Deserialize, Serialize};
use std::sync::Arc;
use tiny_skia::{Path, Point, Rect, Transform};
use typesafe_repository::identity::{GetIdentity, Identity};
use typesafe_repository::macros::Id;
use usvg::Node;
use uuid::Uuid;

#[derive(Debug, Clone, Id)]
#[Id(get_id)]
pub struct Figure {
    pub id: Uuid,
    pub path: Path,
}

impl PartialEq for Figure {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

pub fn path_area(p: &Path) -> f32 {
    let bounds = p.bounds();
    (bounds.right() - bounds.left()) * (bounds.bottom() - bounds.top())
}

impl Figure {
    pub fn new(path: Path) -> Self {
        Self {
            path,
            id: Uuid::new_v4(),
        }
    }
    pub fn bounds(&self) -> Rect {
        self.path.bounds()
    }
    pub fn area(&self) -> f32 {
        path_area(&self.path)
    }
    pub fn is_nested_in(&self, other: &Self) -> bool {
        let a = self.bounds();
        let b = other.bounds();
        a.left() > b.left() && a.right() < b.right() && a.top() > b.top() && a.bottom() < b.bottom()
    }
    pub fn transform(self, transform: Transform) -> Option<Figure> {
        let path = self.path.transform(transform)?;
        Some(Figure { path, id: self.id })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum FigureControl {
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
}

impl FigureControl {
    pub fn rev(&self) -> Self {
        match self {
            Self::TopLeft => Self::BottomRight,
            Self::TopRight => Self::BottomLeft,
            Self::BottomLeft => Self::TopRight,
            Self::BottomRight => Self::TopLeft,
        }
    }
}

pub fn traverse_nodes(n: &[Node]) -> Vec<Figure> {
    let mut lines = vec![];
    for c in n {
        match c {
            Node::Group(g) => {
                let mut res = traverse_nodes(g.children());
                lines.append(&mut res);
            }
            Node::Path(p) => {
                lines.push(Figure::new(p.data().clone()));
            }
            Node::Image(_) => (),
            Node::Text(_) => (),
        }
    }
    lines
}

pub fn sort_by_optimal_path(mut figures: Vec<Arc<Figure>>, start_pos: Point) -> Vec<Arc<Figure>> {
    let nearest = figures
        .iter()
        .map(|f| f.bounds())
        .map(|b| {
            let min_y = (start_pos.y - b.top())
                .abs()
                .min((start_pos.y - b.bottom()).abs());
            let min_x = (start_pos.x - b.left())
                .abs()
                .min((start_pos.x - b.right()).abs());
            (min_y.powi(2) + min_x.powi(2)).sqrt()
        })
        .enumerate()
        .min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal));
    if let Some((index, _)) = nearest {
        let value = figures.remove(index);
        let bounds = value.bounds();
        let start_pos = (
            bounds.left() + bounds.width() / 2.,
            bounds.top() + bounds.height() / 2.,
        )
            .into();
        let mut res = vec![value];
        res.append(&mut sort_by_optimal_path(figures, start_pos));
        res
    } else {
        vec![]
    }
}

pub fn sort_nested(figures: &mut Vec<Arc<Figure>>) {
    let mut fig = figures
        .iter()
        .filter(|f| figures.iter().any(|fig| f.is_nested_in(fig)))
        .cloned()
        .collect::<Vec<_>>();
    fig.sort_by(|a, b| {
        b.area()
            .partial_cmp(&a.area())
            .unwrap_or(std::cmp::Ordering::Equal)
    });
    for f in fig {
        let index = match figures.iter().enumerate().find(|(_, fig)| **fig == f) {
            Some((i, _)) => i,
            None => {
                log::warn!("Unable to find nested figure: {f:?}");
                continue;
            }
        };
        let parent = figures
            .iter()
            .enumerate()
            .filter(|(_, fig)| f.is_nested_in(fig))
            .max_by(|(_, a), (_, b)| {
                b.area()
                    .partial_cmp(&a.area())
                    .unwrap_or(std::cmp::Ordering::Equal)
            });
        let parent = match parent {
            Some((i, _)) => i,
            None => {
                log::warn!("Unable to find parent figure: {parent:?}");
                continue;
            }
        };
        figures.remove(index);
        figures.insert(parent, f);
    }
}

pub fn figures_bounds<'a, T: IntoIterator<Item = &'a F>, F: 'a + std::borrow::Borrow<Figure>>(
    figures: T,
) -> Option<Rect>
where
    <T as IntoIterator>::IntoIter: Clone,
{
    let cmp = |a: &f32, b: &f32| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal);

    let bounds = figures.into_iter().map(|f| f.borrow().bounds());
    let left = bounds.clone().map(|b| b.left()).min_by(cmp)?;
    let right = bounds.clone().map(|b| b.right()).max_by(cmp)?;
    let top = bounds.clone().map(|b| b.top()).min_by(cmp)?;
    let bottom = bounds.clone().map(|b| b.bottom()).max_by(cmp)?;
    Rect::from_ltrb(left, top, right, bottom)
}
