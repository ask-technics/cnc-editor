use super::*;

#[actix::test]
async fn init() {
    let component = Application::new().unwrap();
    component.hide();
    let component_handle = component.as_weak();

    let h = component_handle.clone();
    let arbiter = actix::Arbiter::new();
    let notification_service =
        NotificationService::start_in_arbiter(&arbiter.handle(), move |_| {
            NotificationService::new(h)
        });

    let h = component_handle.clone();
    let service = ApplicationService::start_in_arbiter(&arbiter.handle(), move |_| {
        ApplicationService::new(h, notification_service)
    });
}
