use actix::{Actor, Addr, AsyncContext, Context, Handler, Message, ResponseActFuture, WrapFuture};
use anyhow::Context as AnyhowContext;
use atomic_enum::atomic_enum;
use figure::{
    figures_bounds, path_area, sort_by_optimal_path, sort_nested, traverse_nodes, Figure,
    FigureControl,
};
use futures::future::Either;
use native_dialog::FileDialog;
use nonempty::NonEmpty;
use piet::{RenderContext, Text, TextLayout, TextLayoutBuilder};
use piet_tiny_skia::Cache;
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use slint::{Image, Rgba8Pixel, SharedPixelBuffer, Weak};
use std::collections::VecDeque;
use std::future::Future;
use std::io::Write;
use std::net::SocketAddr;
use std::net::{IpAddr, Ipv4Addr};
use std::path::PathBuf;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;
use tcp_comm::MoveCommand;
use tcp_comm::{Status, TcpCommand, TcpQuery};
use thiserror::Error;
use tiny_skia::{Paint, Path, PathBuilder, PathSegment, PixmapMut, Point, Rect, Stroke, Transform};
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWriteExt;
use tokio::net::{TcpStream, UdpSocket};
use typesafe_repository::{GetIdentity, IdentityOf};
use usvg::Tree;
use uuid::Uuid;

slint::include_modules!();

pub mod figure;
pub mod notification;
#[cfg(test)]
pub mod tests;

use notification::{DisplayNotification, NotificationService, Notify};

const TRACE_LEN: usize = 6000;

const RULER_WIDTH: u32 = 80;
const BEZIER_STEPS: usize = 70;

const VIEWPORT_WIDTH: u32 = 720;
const VIEWPORT_HEIGHT: u32 = 480;

const DEFAULT_WORKSPACE_WIDTH: u32 = 1000;
const DEFAULT_WORKSPACE_HEIGHT: u32 = 1000;
#[allow(clippy::excessive_precision)]
const DEFAULT_PPMM: f32 = 102.564102;

const FIGURE_CONTROL_WIDTH: f32 = 4.;

const CACHE_FILE: &str = ".cache";
const RECOVERY_FILE: &str = ".cnceditor.json";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FigureDto {
    pub id: Uuid,
    pub path: Vec<PathSegmentDto>,
}

#[derive(Error, Debug)]
pub enum FromFigureDtoError {
    #[error("path is invalid")]
    InvalidPath,
}

impl TryFrom<FigureDto> for Figure {
    type Error = FromFigureDtoError;

    fn try_from(f: FigureDto) -> Result<Self, Self::Error> {
        let mut path = tiny_skia::PathBuilder::new();
        for s in f.path.into_iter().map(Into::into) {
            match s {
                PathSegment::MoveTo(Point { x, y }) => path.move_to(x, y),
                PathSegment::LineTo(Point { x, y }) => path.line_to(x, y),
                PathSegment::QuadTo(a, b) => path.quad_to(a.x, a.y, b.x, b.y),
                PathSegment::CubicTo(a, b, c) => path.cubic_to(a.x, a.y, b.x, b.y, c.x, c.y),
                PathSegment::Close => path.close(),
            }
        }
        let path = path.finish().ok_or(FromFigureDtoError::InvalidPath)?;
        let id = f.id;
        Ok(Figure { path, id })
    }
}

impl From<Figure> for FigureDto {
    fn from(f: Figure) -> Self {
        let path = f.path.segments().map(Into::into).collect();
        let id = f.id;
        Self { path, id }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PointDto {
    x: f32,
    y: f32,
}

impl From<PointDto> for Point {
    fn from(PointDto { x, y }: PointDto) -> Self {
        Point { x, y }
    }
}

impl From<Point> for PointDto {
    fn from(Point { x, y }: Point) -> Self {
        Self { x, y }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum PathSegmentDto {
    MoveTo(PointDto),
    LineTo(PointDto),
    QuadTo(PointDto, PointDto),
    CubicTo(PointDto, PointDto, PointDto),
    Close,
}

impl From<PathSegmentDto> for PathSegment {
    fn from(p: PathSegmentDto) -> Self {
        match p {
            PathSegmentDto::MoveTo(to) => PathSegment::MoveTo(to.into()),
            PathSegmentDto::LineTo(to) => PathSegment::LineTo(to.into()),
            PathSegmentDto::QuadTo(a, b) => PathSegment::QuadTo(a.into(), b.into()),
            PathSegmentDto::CubicTo(a, b, c) => PathSegment::CubicTo(a.into(), b.into(), c.into()),
            PathSegmentDto::Close => PathSegment::Close,
        }
    }
}

impl From<PathSegment> for PathSegmentDto {
    fn from(p: PathSegment) -> Self {
        match p {
            PathSegment::MoveTo(to) => PathSegmentDto::MoveTo(to.into()),
            PathSegment::LineTo(to) => PathSegmentDto::LineTo(to.into()),
            PathSegment::QuadTo(a, b) => PathSegmentDto::QuadTo(a.into(), b.into()),
            PathSegment::CubicTo(a, b, c) => PathSegmentDto::CubicTo(a.into(), b.into(), c.into()),
            PathSegment::Close => PathSegmentDto::Close,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum ResizeControl {
    Control(FigureControl),
    Center,
}

impl ResizeControl {
    pub fn center() -> Self {
        Self::Center
    }
    pub fn new<T: Into<Option<FigureControl>>>(t: T) -> Self {
        match t.into() {
            Some(c) => Self::Control(c),
            None => Self::Center,
        }
    }
    pub fn point_of(&self, bounds: Rect) -> Point {
        match self {
            Self::Control(FigureControl::TopLeft) => (bounds.left(), bounds.top()).into(),
            Self::Control(FigureControl::TopRight) => (bounds.right(), bounds.top()).into(),
            Self::Control(FigureControl::BottomLeft) => (bounds.left(), bounds.bottom()).into(),
            Self::Control(FigureControl::BottomRight) => (bounds.right(), bounds.bottom()).into(),
            Self::Center => (
                bounds.left() + bounds.width() / 2.,
                bounds.top() + bounds.height() / 2.,
            )
                .into(),
        }
    }
    pub fn rev(&self) -> Self {
        match self {
            Self::Control(c) => Self::Control(c.rev()),
            Self::Center => Self::Center,
        }
    }
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct DragRuler(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct EndDragRuler(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct Simulate;

#[derive(Message)]
#[rtype(result = "()")]
pub struct ChangeScale(pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct IncreaseScale(pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct DecreaseScale(pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct Redraw(pub Option<(f32, f32)>);

#[derive(Message)]
#[rtype(result = "()")]
pub struct RedrawDevice;

#[derive(Message)]
#[rtype(result = "()")]
pub struct Drag(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct Resize(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct EndResize;

#[derive(Message)]
#[rtype(result = "()")]
pub struct EndDrag;

#[derive(Message)]
#[rtype(result = "()")]
pub struct AddFigure(pub Figure);

#[derive(Message)]
#[rtype(result = "()")]
pub struct AddFigures(pub Vec<Figure>);

#[derive(Message)]
#[rtype(result = "()")]
pub struct SelectFigure(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct AddFigureToSelection(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct MoveSelected(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct EndMoveSelected;

#[derive(Message)]
#[rtype(result = "()")]
pub struct SelectAll;

#[derive(Message)]
#[rtype(result = "()")]
pub struct GenerateGcode;

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetPpmm(pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct Clear;

#[derive(Message)]
#[rtype(result = "()")]
pub struct ChangeOrderOfSelected(pub usize);

#[derive(Message)]
#[rtype(result = "()")]
pub struct ArrangeSelected;

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetDisplayOrder(pub bool);

#[derive(Message)]
#[rtype(result = "()")]
pub struct DragSelectionBox(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct EndDragSelectionBox;

#[derive(Message)]
#[rtype(result = "()")]
pub struct Hover {
    pub x: f32,
    pub y: f32,
    pub control_pressed: bool,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct Save;

#[derive(Message)]
#[rtype(result = "()")]
pub struct Open;

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetWidth(pub u32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetHeight(pub u32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetSelectedWidth(pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetSelectedHeight(pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct AlignSelected(pub FigureAlignment);

#[derive(Message)]
#[rtype(result = "()")]
pub struct MouseUp(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct Rotate(pub f32, pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct HistoryBack;

#[derive(Message)]
#[rtype(result = "()")]
pub struct HistoryForward;

#[derive(Message)]
#[rtype(result = "()")]
pub struct Connect(Option<IpAddr>);

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetDisconnected;

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetCutRate(pub f32);

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetWindowTitle(pub Option<String>);

#[derive(Message)]
#[rtype(result = "()")]
pub struct EnableDeviceLocation;

#[derive(Message)]
#[rtype(result = "()")]
pub struct DisableDeviceLocation;

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetDeviceStatus(pub Option<DeviceStatus>);

pub enum Movement {
    Up,
    Left,
    Right,
    Down,
}

#[derive(Message)]
#[rtype(result = "()")]
pub struct DeviceMove(pub Movement);

#[derive(Message)]
#[rtype(result = "()")]
pub struct DeviceExecute;

#[derive(Message)]
#[rtype(result = "()")]
pub struct DeviceStop;

#[derive(Message)]
#[rtype(result = "()")]
pub struct DevicePause;

#[derive(Message)]
#[rtype(result = "()")]
pub struct DeviceResume;

#[derive(Message)]
#[rtype(result = "()")]
pub struct SearchForDevice;

#[derive(Message)]
#[rtype(result = "()")]
pub struct SaveState;

#[derive(Message)]
#[rtype(result = "()")]
pub struct SetWindowSize(pub f32, pub f32);

/// mm/sec
pub type CutRate = f32;

pub struct ApplicationService {
    handle: Weak<Application>,
    figures: Vec<Arc<Figure>>,
    selected_figures: Option<NonEmpty<Arc<Figure>>>,
    scale: f32,
    ruler_buffer: SharedPixelBuffer<Rgba8Pixel>,
    canvas_buffer: SharedPixelBuffer<Rgba8Pixel>,
    file: Option<PathBuf>,
    history_buf: Option<HistoryEntry>,
    history: VecDeque<Arc<HistoryEntry>>,
    history_offset: usize,
    width: u32,
    height: u32,
    ppmm: f32,
    ruler: Option<(f32, f32)>,
    last_drag: Option<(f32, f32)>,
    action: Option<Action>,
    x: f32,
    y: f32,
    display_order: bool,
    cache: piet_tiny_skia::Cache,
    options: ApplicationOptions,
    upload_options: Option<UploadOptions>,
    notification_service: Addr<NotificationService>,
    cut_rate: CutRate,
    viewport_width: u32,
    viewport_height: u32,
    device_status: Option<(DeviceStatus, Vec<(f32, f32)>)>,
    window_width: u32,
    window_height: u32,
}

#[derive(Serialize, Deserialize)]
pub struct ApplicationState {
    file: Option<PathBuf>,
    window_width: u32,
    window_height: u32,
}

#[derive(Debug, Clone, PartialEq)]
pub struct DeviceStatus {
    x: f32,
    y: f32,
    status: Status,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ApplicationOptions {
    history_size: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UploadOptions {
    addr: IpAddr,
    port: u16,
}

impl Default for UploadOptions {
    fn default() -> Self {
        Self {
            addr: IpAddr::V4(Ipv4Addr::LOCALHOST),
            port: 8080,
        }
    }
}

impl Default for ApplicationOptions {
    fn default() -> Self {
        Self { history_size: 10 }
    }
}

pub struct RotationState {
    pub point: Point,
    pub last_angle: Option<f32>,
}

#[derive(Debug, Clone)]
pub struct HistoryEntry {
    operation: Operation,
    selected: Option<NonEmpty<IdentityOf<Figure>>>,
    width: u32,
    height: u32,
    ppmm: f32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct HistoryEntryDto {
    operation: OperationDto,
    selected: Vec<IdentityOf<Figure>>,
    width: u32,
    height: u32,
    ppmm: f32,
}

impl From<HistoryEntry> for HistoryEntryDto {
    fn from(e: HistoryEntry) -> Self {
        Self {
            operation: e.operation.into(),
            selected: e
                .selected
                .as_ref()
                .map(|f| f.iter().cloned().collect())
                .unwrap_or_default(),
            width: e.width,
            height: e.height,
            ppmm: e.ppmm,
        }
    }
}

impl TryFrom<HistoryEntryDto> for HistoryEntry {
    type Error = anyhow::Error;

    fn try_from(e: HistoryEntryDto) -> Result<Self, Self::Error> {
        Ok(Self {
            operation: e.operation.try_into()?,
            selected: NonEmpty::collect(e.selected),
            width: e.width,
            height: e.height,
            ppmm: e.ppmm,
        })
    }
}

impl HistoryEntry {
    pub fn try_add(&self, other: &Self) -> Option<HistoryEntry> {
        if self.width != other.width
            || self.height != other.height
            || self.ppmm != other.ppmm
            || self.selected != other.selected
        {
            return None;
        }
        Some(Self {
            operation: self.operation.try_add(&other.operation)?,
            ..self.clone()
        })
    }
}

#[derive(Debug)]
pub enum Action {
    Resize(ResizeControl),
    Rotate {
        point: Point,
        last_angle: Option<f32>,
    },
    Move(Point, Option<Point>),
    Ruler(Point),
    SelectionBox(Point, Option<Point>),
}

impl Action {
    pub fn is_active(&self) -> bool {
        !matches!(self, Action::SelectionBox(_, None))
    }
}

#[derive(Debug, Clone)]
pub enum Operation {
    Resize { control: ResizeControl, ratio: f32 },
    Move { delta_x: f32, delta_y: f32 },
    Rotate { point: Point, angle: f32 },
    AddFigure(Arc<Figure>),
    RemoveFigure(Arc<Figure>),
    ChangeSelection,
    ChangeOrder,
    ChangeWorkspace,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum OperationDto {
    Resize { control: ResizeControl, ratio: f32 },
    Move { delta_x: f32, delta_y: f32 },
    Rotate { point: PointDto, angle: f32 },
    AddFigure(FigureDto),
    RemoveFigure(FigureDto),
    ChangeSelection,
    ChangeOrder,
    ChangeWorkspace,
}

impl From<Operation> for OperationDto {
    fn from(o: Operation) -> Self {
        match o {
            Operation::Resize { control, ratio } => Self::Resize { control, ratio },
            Operation::Move { delta_x, delta_y } => Self::Move { delta_x, delta_y },
            Operation::Rotate { point, angle } => Self::Rotate {
                point: point.into(),
                angle,
            },
            Operation::AddFigure(fig) => Self::AddFigure(Arc::unwrap_or_clone(fig).into()),
            Operation::RemoveFigure(fig) => Self::RemoveFigure(Arc::unwrap_or_clone(fig).into()),
            Operation::ChangeSelection => Self::ChangeSelection,
            Operation::ChangeOrder => Self::ChangeOrder,
            Operation::ChangeWorkspace => Self::ChangeWorkspace,
        }
    }
}

impl TryFrom<OperationDto> for Operation {
    type Error = FromFigureDtoError;
    fn try_from(o: OperationDto) -> Result<Self, Self::Error> {
        let o = match o {
            OperationDto::Resize { control, ratio } => Self::Resize { control, ratio },
            OperationDto::Move { delta_x, delta_y } => Self::Move { delta_x, delta_y },
            OperationDto::Rotate { point, angle } => Self::Rotate {
                point: point.into(),
                angle,
            },
            OperationDto::AddFigure(fig) => Self::AddFigure(Arc::new(fig.try_into()?)),
            OperationDto::RemoveFigure(fig) => Self::RemoveFigure(Arc::new(fig.try_into()?)),
            OperationDto::ChangeSelection => Self::ChangeSelection,
            OperationDto::ChangeOrder => Self::ChangeOrder,
            OperationDto::ChangeWorkspace => Self::ChangeWorkspace,
        };
        Ok(o)
    }
}

impl Operation {
    pub fn as_transform(self) -> Option<Transform> {
        match self {
            Operation::Resize { ratio, .. } => Some(Transform::from_scale(ratio, ratio)),
            Operation::Move { delta_x, delta_y } => {
                Some(Transform::from_translate(delta_x, delta_y))
            }
            Operation::Rotate {
                point: Point { x, y },
                angle,
            } => Some(Transform::from_rotate_at(angle, x, y)),
            _ => None,
        }
    }
    pub fn rev(self) -> Self {
        match self {
            Self::Resize { control, ratio } => Self::Resize {
                control: control.rev(),
                ratio: 1. / ratio,
            },
            Self::Move { delta_x, delta_y } => Self::Move {
                delta_x: -delta_x,
                delta_y: -delta_y,
            },
            Self::Rotate { point, angle } => Self::Rotate {
                point,
                angle: -angle,
            },
            Self::AddFigure(f) => Self::RemoveFigure(f),
            Self::RemoveFigure(f) => Self::AddFigure(f),
            Self::ChangeSelection => Self::ChangeSelection,
            Self::ChangeOrder => Self::ChangeOrder,
            Self::ChangeWorkspace => Self::ChangeWorkspace,
        }
    }
    pub fn try_add(&self, other: &Self) -> Option<Operation> {
        match (self, other) {
            (
                Operation::Resize {
                    control: c1,
                    ratio: r1,
                },
                Operation::Resize {
                    control: c2,
                    ratio: r2,
                },
            ) if c1 == c2 => Some(Operation::Resize {
                control: c1.clone(),
                ratio: r1 * r2,
            }),
            (
                Operation::Move {
                    delta_x: dx1,
                    delta_y: dy1,
                },
                Operation::Move {
                    delta_x: dx2,
                    delta_y: dy2,
                },
            ) => Some(Operation::Move {
                delta_x: dx1 + dx2,
                delta_y: dy1 + dy2,
            }),
            (
                Operation::Rotate {
                    point: p1,
                    angle: a1,
                },
                Operation::Rotate {
                    point: p2,
                    angle: a2,
                },
            ) if p1 == p2 => Some(Operation::Rotate {
                point: *p1,
                angle: a1 + a2,
            }),
            _ => None,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct ApplicationSettings {
    file: Option<PathBuf>,
    options: ApplicationOptionsDto,
    upload_options: UploadOptionsDto,
}

#[derive(Serialize, Deserialize)]
pub struct ApplicationOptionsDto {
    history_size: usize,
}

impl From<ApplicationOptionsDto> for ApplicationOptions {
    fn from(val: ApplicationOptionsDto) -> Self {
        ApplicationOptions {
            history_size: val.history_size,
        }
    }
}

impl From<ApplicationOptions> for ApplicationOptionsDto {
    fn from(ApplicationOptions { history_size }: ApplicationOptions) -> Self {
        Self { history_size }
    }
}

#[derive(Serialize, Deserialize)]
pub struct UploadOptionsDto {
    addr: IpAddr,
    port: u16,
}

impl From<UploadOptionsDto> for UploadOptions {
    fn from(val: UploadOptionsDto) -> Self {
        UploadOptions {
            addr: val.addr,
            port: val.port,
        }
    }
}

impl From<UploadOptions> for UploadOptionsDto {
    fn from(UploadOptions { addr, port }: UploadOptions) -> Self {
        Self { addr, port }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Project {
    figures: Vec<FigureDto>,
    selected_figures: Vec<FigureDto>,
    scale: f32,
    file: Option<PathBuf>,
    history_buf: Option<HistoryEntryDto>,
    history: VecDeque<HistoryEntryDto>,
    history_offset: usize,
    width: u32,
    height: u32,
    ppmm: f32,
    x: f32,
    y: f32,
    display_order: bool,
    options: ApplicationOptions,
    upload_options: Option<UploadOptions>,
    cut_rate: CutRate,
}

impl ApplicationService {
    pub fn new(handle: Weak<Application>, notification_service: Addr<NotificationService>) -> Self {
        let width = DEFAULT_WORKSPACE_WIDTH;
        let height = DEFAULT_WORKSPACE_HEIGHT;
        let viewport_width = VIEWPORT_WIDTH;
        let viewport_height = VIEWPORT_HEIGHT;
        let ppmm = DEFAULT_PPMM;
        handle
            .upgrade_in_event_loop(move |handle| {
                handle.set_workspace_width(width.to_string().into());
                handle.set_workspace_height(height.to_string().into());
                handle.set_ppmm(ppmm);
            })
            .ok_or_notify(&notification_service);
        let scale = 0.1;
        let y = 0.;
        let x = 0.;
        let canvas_buffer = SharedPixelBuffer::<Rgba8Pixel>::new(viewport_width, viewport_height);
        let ruler_buffer = SharedPixelBuffer::<Rgba8Pixel>::new(viewport_width, viewport_height);
        let mut cache = piet_tiny_skia::Cache::new();

        let mut rulers_buffer =
            SharedPixelBuffer::<Rgba8Pixel>::new(viewport_width + RULER_WIDTH, RULER_WIDTH);
        draw_rulers_horizontal(
            &mut rulers_buffer,
            viewport_width,
            scale,
            ppmm,
            &mut cache,
            x,
            viewport_width,
        )
        .ok_or_notify(&notification_service);

        let mut vertical_ruler_buffer =
            SharedPixelBuffer::<Rgba8Pixel>::new(RULER_WIDTH, viewport_height + RULER_WIDTH);
        draw_rulers_vertical(
            &mut vertical_ruler_buffer,
            viewport_height,
            scale,
            ppmm,
            &mut cache,
            y,
            viewport_height,
        )
        .ok_or_notify(&notification_service);

        handle
            .upgrade_in_event_loop(move |handle| {
                let image = Image::from_rgba8_premultiplied(rulers_buffer);
                handle.set_horizontal_ruler(image);
                let image = Image::from_rgba8_premultiplied(vertical_ruler_buffer);
                handle.set_vertical_ruler(image);
            })
            .ok_or_notify(&notification_service);
        Self {
            handle,
            scale,
            canvas_buffer,
            ruler_buffer,
            selected_figures: None,
            ruler: None,
            width,
            height,
            history_buf: None,
            history: VecDeque::new(),
            history_offset: 0,
            file: None,
            ppmm,
            figures: vec![],
            last_drag: None,
            display_order: true,
            cache,
            action: None,
            x,
            y,
            options: ApplicationOptions::default(),
            upload_options: None,
            notification_service,
            cut_rate: 10.,
            viewport_width,
            viewport_height,
            device_status: None,
            window_height: 0,
            window_width: 0,
        }
    }

    fn add_figures(&mut self, figures: Vec<Figure>) {
        let cmp = |a: &f32, b: &f32| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal);
        let bottom = figures.iter().map(|f| f.bounds().bottom()).max_by(cmp);
        let figures = match bottom {
            Some(_bottom) => {
                let scale = 1.;
                let delta = 0.;
                figures
                    .into_iter()
                    .map(|f| {
                        let p = f.path;
                        let id = f.id;
                        let mut path = tiny_skia::PathBuilder::new();
                        for s in p.segments() {
                            match s {
                                PathSegment::MoveTo(Point { x, y }) => {
                                    path.move_to(x * scale, y * scale + delta);
                                }
                                PathSegment::LineTo(Point { x, y }) => {
                                    path.line_to(x * scale, y * scale + delta);
                                }
                                PathSegment::QuadTo(p1, p2) => {
                                    path.quad_to(
                                        p1.x * scale,
                                        p1.y * scale + delta,
                                        p2.x * scale,
                                        p2.y * scale + delta,
                                    );
                                }
                                PathSegment::CubicTo(p1, p2, p3) => {
                                    path.cubic_to(
                                        p1.x * scale,
                                        p1.y * scale + delta,
                                        p2.x * scale,
                                        p2.y * scale + delta,
                                        p3.x * scale,
                                        p3.y * scale + delta,
                                    );
                                }
                                PathSegment::Close => {
                                    path.close();
                                }
                            }
                        }
                        let path = path.finish().unwrap();
                        Figure { path, id }
                    })
                    .collect()
            }
            None => figures,
        };
        self.figures
            .append(&mut figures.into_iter().map(Arc::new).collect());
    }

    fn open_file(&mut self, path: &std::path::Path) -> Result<(), anyhow::Error> {
        let r = std::fs::read_to_string(path)?;
        match path.extension() {
            Some(x) if x == "svg" => {
                self.figures = vec![];
                self.selected_figures = None;
                let opts = usvg::Options {
                    dpi: 10.,
                    default_size: usvg::Size::from_wh(300., 300.).unwrap(),
                    ..Default::default()
                };
                let tree = Tree::from_str(&r, &opts)?;
                let res = traverse_nodes(tree.root().children());
                self.add_figures(res);
                Ok(())
            }
            Some(x) if x == "json" => {
                let state: Project = serde_json::from_str(&r)?;
                self.apply_project(state)
                    .ok_or_notify(&self.notification_service);
                Ok(())
            }
            _ => Ok(()),
        }
    }

    fn save_file<P: AsRef<std::path::Path>>(&mut self, path: P) -> Result<(), anyhow::Error> {
        let path = path.as_ref();
        let project = Project {
            figures: self
                .figures
                .iter()
                .cloned()
                .map(Arc::unwrap_or_clone)
                .map(Into::into)
                .collect(),
            selected_figures: self
                .selected_figures
                .as_ref()
                .map(|f| {
                    f.iter()
                        .cloned()
                        .map(Arc::unwrap_or_clone)
                        .map(Into::into)
                        .collect()
                })
                .unwrap_or_default(),
            scale: self.scale,
            file: self.file.clone(),
            history_buf: self.history_buf.clone().map(Into::into),
            history: self
                .history
                .iter()
                .cloned()
                .map(Arc::unwrap_or_clone)
                .map(Into::into)
                .collect(),
            history_offset: self.history_offset,
            cut_rate: self.cut_rate,
            display_order: self.display_order,
            height: self.height,
            width: self.width,
            options: self.options.clone(),
            ppmm: self.ppmm,
            upload_options: self.upload_options.clone(),
            x: self.x,
            y: self.y,
        };
        let r = serde_json::to_string(&project)?;
        std::fs::write(path, r)?;
        Ok(())
    }

    fn read_cached_settings(&mut self) -> Result<ApplicationState, anyhow::Error> {
        let file = std::fs::File::open(CACHE_FILE)?;
        Ok(serde_json::from_reader(file)?)
    }

    fn apply_project(&mut self, settings: Project) -> Result<(), anyhow::Error> {
        let figures: Vec<_> = settings
            .figures
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<_, _>>()?;
        self.figures = figures.into_iter().map(Arc::new).collect();
        let selected_figures: Vec<_> = settings
            .selected_figures
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<_, _>>()?;
        self.selected_figures = NonEmpty::collect(selected_figures.into_iter().map(Arc::new));
        self.scale = settings.scale;
        self.history_buf = settings.history_buf.map(TryInto::try_into).transpose()?;
        let history = settings
            .history
            .into_iter()
            .map(TryInto::try_into)
            .collect::<Result<Vec<HistoryEntry>, _>>()?;
        self.history = history.into_iter().map(Arc::new).collect();
        self.history_offset = settings.history_offset;
        self.width = settings.width;
        self.height = settings.height;
        self.ppmm = settings.ppmm;
        self.x = settings.x;
        self.y = settings.y;
        self.display_order = settings.display_order;
        self.options = settings.options;
        self.upload_options = settings.upload_options;
        self.cut_rate = settings.cut_rate;

        self.handle
            .upgrade_in_event_loop(move |h| {
                h.set_cut_rate(settings.cut_rate);
                h.set_ppmm(settings.ppmm);
                h.set_display_order(settings.display_order);
            })
            .ok_or_notify(&self.notification_service);
        Ok(())
    }

    fn write_cached_settings(&self) -> Result<(), anyhow::Error> {
        if let Some(file) = self.file.clone() {
            let state = ApplicationState {
                file: Some(file),
                window_width: self.window_width,
                window_height: self.window_height,
            };
            let content = serde_json::to_string(&state)?;
            std::fs::write(CACHE_FILE, content)?;
        } else {
            let project = Project {
                figures: self
                    .figures
                    .iter()
                    .cloned()
                    .map(Arc::unwrap_or_clone)
                    .map(Into::into)
                    .collect(),
                selected_figures: self
                    .selected_figures
                    .as_ref()
                    .map(|f| {
                        f.iter()
                            .cloned()
                            .map(Arc::unwrap_or_clone)
                            .map(Into::into)
                            .collect()
                    })
                    .unwrap_or_default(),
                scale: self.scale,
                file: self.file.clone(),
                history_buf: self.history_buf.clone().map(Into::into),
                history: self
                    .history
                    .iter()
                    .cloned()
                    .map(Arc::unwrap_or_clone)
                    .map(Into::into)
                    .collect(),
                history_offset: self.history_offset,
                cut_rate: self.cut_rate,
                display_order: self.display_order,
                height: self.height,
                width: self.width,
                options: self.options.clone(),
                ppmm: self.ppmm,
                upload_options: self.upload_options.clone(),
                x: self.x,
                y: self.y,
            };
            let content = serde_json::to_string(&project)?;
            std::fs::write(RECOVERY_FILE, content)?;
            let state = ApplicationState {
                file: Some(RECOVERY_FILE.into()),
                window_width: self.window_width,
                window_height: self.window_height,
            };
            let content = serde_json::to_string(&state)?;
            std::fs::write(CACHE_FILE, content)?;
        }
        Ok(())
    }

    pub fn render_figures<'a, T: IntoIterator<Item = &'a Arc<Figure>>>(
        mut pixmap: PixmapMut,
        transform: Transform,
        width: u32,
        height: u32,
        ppmm: f32,
        scale: f32,
        figures: T,
    ) -> PixmapMut {
        let mut line = tiny_skia::PathBuilder::new();
        line.move_to(0., 0.);
        line.line_to(width as f32 * ppmm, 0.);
        line.line_to(width as f32 * ppmm, height as f32 * ppmm);
        line.line_to(0., height as f32 * ppmm);
        line.close();
        let line = line.finish().unwrap();

        pixmap.stroke_path(
            &line,
            &Paint {
                ..Default::default()
            },
            &tiny_skia::Stroke {
                width: 1. / scale,
                ..Default::default()
            },
            transform,
            None,
        );
        for figure in figures {
            pixmap.stroke_path(
                &figure.path,
                &Paint {
                    ..Default::default()
                },
                &tiny_skia::Stroke {
                    width: 1. / scale,
                    ..Default::default()
                },
                transform,
                None,
            );
        }
        pixmap
    }

    pub fn render_order<'a, T: IntoIterator<Item = &'a Arc<Figure>>>(
        mut pixmap: PixmapMut<'a>,
        cache: &mut Cache,
        x_p: f32,
        y_p: f32,
        scale: f32,
        figures: T,
    ) -> PixmapMut<'a> {
        for (i, figure) in figures.into_iter().enumerate() {
            let mut rc = cache.render_context(&mut pixmap);

            let width = (figure.bounds().width() * scale) as f64;
            let height = (figure.bounds().height() * scale) as f64;
            let text = (i + 1).to_string();
            let text_len = text.len() as f64 + 1.;
            let text = rc
                .text()
                .new_text_layout(text)
                .font(
                    piet::FontFamily::MONOSPACE,
                    100_f64.min(width.max(height / text_len)),
                )
                .text_color(piet::Color::BLUE)
                .build()
                .unwrap();
            let bounds = figure.bounds();
            let x = (bounds.left() + bounds.width() / 2.) as f64;
            let y = (bounds.top() + bounds.height() / 2.) as f64;
            let x = (x - x_p as f64) * scale as f64;
            let y = (y - y_p as f64) * scale as f64;
            let x = x - text.size().width / 2.;
            let y = y - text.size().height / 2.;
            rc.draw_text(&text, piet::kurbo::Point { x, y });
        }
        pixmap
    }

    pub fn map_selected<F: Fn(&Figure) -> Figure>(&mut self, func: F) {
        let selected_figures = match &mut self.selected_figures {
            Some(s) => s,
            None => return,
        };
        for f in selected_figures.iter_mut() {
            let new_figure = Arc::new(func(f));
            if let Some(f) = self.figures.iter_mut().find(|fig| *fig == f) {
                *f = new_figure.clone();
            }
            *f = new_figure;
        }
    }
    pub fn map_by_ids<F: Fn(&Figure) -> Figure>(&mut self, func: F, ids: &[IdentityOf<Figure>]) {
        let figures = self.figures.iter_mut().filter(|f| ids.contains(&f.id()));
        for f in figures {
            let new_figure = Arc::new(func(f));
            if let Some(f) = self
                .selected_figures
                .as_mut()
                .and_then(|l| l.iter_mut().find(|fig| *fig == f))
            {
                *f = new_figure.clone();
            }
            *f = new_figure;
        }
    }
    pub fn transform_by_ids(&mut self, transform: Transform, ids: &[IdentityOf<Figure>]) {
        self.map_by_ids(|f| f.clone().transform(transform).unwrap(), ids)
    }
    pub fn transform_selected(&mut self, transform: Transform) {
        self.map_selected(|f| f.clone().transform(transform).unwrap())
    }
    pub fn resize_selected(
        &mut self,
        control: ResizeControl,
        mut ratio: f32,
    ) -> Result<(), anyhow::Error> {
        let bounds = match self.selection_bounds() {
            Some(x) => x,
            None => {
                log::warn!("Attempt to resize selected when no figures are selected");
                return Ok(());
            }
        };
        let point = control.point_of(bounds);
        let translate_x = point.x * ratio - point.x;
        let translate_y = point.y * ratio - point.y;
        if ((bounds.left() * ratio - translate_x) as isize) < 0 {
            let resize_width = (0. - point.x).abs();
            ratio = ratio.min(resize_width / bounds.width());
        }
        if ((bounds.top() * ratio - translate_y) as isize) < 0 {
            let resize_height = (0. - point.y).abs();
            ratio = ratio.min(resize_height / bounds.height());
        }
        let workspace_width = self.width as f32 * self.ppmm;
        if ((bounds.right() * ratio - translate_x) as usize) > workspace_width as usize {
            let resize_width = (workspace_width - point.x).abs();
            ratio = ratio.min(resize_width / bounds.width());
        }
        let workspace_height = self.height as f32 * self.ppmm;
        if ((bounds.bottom() * ratio - translate_y) as usize) > workspace_height as usize {
            let resize_height = (workspace_height - point.y).abs();
            ratio = ratio.min(resize_height / bounds.height());
        }
        let translate_x = point.x * ratio - point.x;
        let translate_y = point.y * ratio - point.y;
        self.map_selected(|f| {
            f.clone()
                .transform(
                    Transform::from_scale(ratio, ratio).post_translate(-translate_x, -translate_y),
                )
                .unwrap()
        });
        let w = bounds.width() * ratio / self.ppmm;
        let h = bounds.height() * ratio / self.ppmm;
        self.buf_history(Operation::Resize { control, ratio });
        self.handle.upgrade_in_event_loop(move |handle| {
            handle.set_selected_width(format!("{w:.2}").into());
            handle.set_selected_height(format!("{h:.2}").into());
        })?;
        Ok(())
    }
    pub fn move_selected(&mut self, (delta_x, delta_y): (f32, f32)) {
        let bounds = match self.selection_bounds() {
            Some(b) => b,
            None => return,
        };
        let (delta_x, delta_y) = bound_move(
            bounds,
            (delta_x, delta_y),
            self.width,
            self.height,
            self.ppmm,
        );
        self.buf_history(Operation::Move { delta_x, delta_y });
        self.transform_selected(Transform::from_translate(delta_x, delta_y));
    }
    pub fn selection_bounds(&self) -> Option<Rect> {
        figures_bounds(self.selected_figures.as_ref()?)
    }
    pub fn without_figure<R>(
        &mut self,
        figure: Arc<Figure>,
        f: impl Fn(&mut Self) -> R,
    ) -> Option<R> {
        let index = self
            .selected_figures
            .as_ref()
            .and_then(|s| s.iter().cloned().enumerate().find(|(_, f)| *f == figure));
        if let Some((selected_figures, (index, figure))) = self.selected_figures.as_mut().zip(index)
        {
            let mut vec = selected_figures.iter().cloned().collect::<Vec<_>>();
            vec.remove(index);
            self.selected_figures = NonEmpty::from_vec(vec);
            let res = f(self);
            if let Some(selected_figures) = &mut self.selected_figures {
                selected_figures.insert(index, figure.clone());
            }
            Some(res)
        } else {
            None
        }
    }
    pub fn history_entry(&self, operation: Operation) -> HistoryEntry {
        HistoryEntry {
            operation,
            selected: self
                .selected_figures
                .as_ref()
                .and_then(|l| NonEmpty::from_vec(l.iter().map(|f| f.id()).collect())),
            width: self.width,
            height: self.height,
            ppmm: self.ppmm,
        }
    }
    pub fn buf_history(&mut self, operation: Operation) {
        let e = self.history_entry(operation);
        if let Some(buf) = &self.history_buf {
            self.history_buf = buf.try_add(&e);
        } else {
            self.history_buf = Some(e);
        }
    }
    pub fn set_title(&self, title: impl Into<Option<String>>) {
        let title = title.into();
        self.handle
            .upgrade_in_event_loop(move |handle| {
                let title = match title {
                    Some(title) => format!("{} :: CNC editor", title.trim()).into(),
                    None => "CNC editor".into(),
                };
                handle.set_window_title(title);
            })
            .ok_or_notify(&self.notification_service);
    }
    pub fn estimate_cut_time(&mut self) {
        let time = cut_time(&self.figures, self.cut_rate, self.ppmm);
        self.handle
            .upgrade_in_event_loop(move |handle| {
                handle.set_estimated_time(format!("{:.2}сек.", time / 1000.).into());
            })
            .ok_or_notify(&self.notification_service);
    }
    pub fn push_history(&mut self, e: HistoryEntry) {
        for _ in 0..self.history_offset {
            self.history.pop_back();
        }
        self.history_offset = 0;
        self.history.push_back(Arc::new(e));
        if self.history.len() > self.options.history_size {
            self.history.pop_front();
        }
        let (b, f) = match (self.history.len(), self.history_offset) {
            (0, _) => (false, false),
            (l, o) => (o < l, o > 0),
        };
        let time = cut_time(&self.figures, self.cut_rate, self.ppmm);
        if let Some(file) = &self.file {
            let filename = file.file_name().unwrap().to_str().unwrap();
            self.set_title(format!("{filename}*"));
        } else {
            self.set_title("Untitled*".to_string());
        }
        self.handle
            .upgrade_in_event_loop(move |handle| {
                handle.set_history_available_back(b);
                handle.set_history_available_forward(f);
                handle.set_estimated_time(format!("{:.2}сек.", time / 1000.).into());
            })
            .ok_or_notify(&self.notification_service);
    }
    pub fn push_history_from_buf(&mut self) {
        let mut e = None;
        std::mem::swap(&mut e, &mut self.history_buf);
        if let Some(buf) = e {
            self.push_history(buf.clone());
        }
    }
    pub fn apply_history_entry_rev(&mut self, e: &HistoryEntry) {
        let transform = e.operation.clone().rev().as_transform();
        if let Some((selected, transform)) = e.selected.clone().zip(transform) {
            self.transform_by_ids(transform, &selected.into_iter().collect::<Vec<_>>());
        }
    }
    pub fn apply_history_entry(&mut self, e: &HistoryEntry) {
        let transform = e.operation.clone().as_transform();
        if let Some((selected, transform)) = e.selected.clone().zip(transform) {
            self.transform_by_ids(transform, &selected.into_iter().collect::<Vec<_>>());
        }
    }
}

impl Actor for ApplicationService {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        match self.read_cached_settings() {
            Ok(s) => {
                self.handle
                    .upgrade_in_event_loop(move |h| {
                        h.window().set_size(slint::PhysicalSize {
                            width: s.window_width,
                            height: s.window_height,
                        });
                    })
                    .ok_or_notify(&self.notification_service);
                if let Some(file) = s.file {
                    self.open_file(&file).unwrap();
                } else {
                    self.open_file(&PathBuf::from("ask.svg")).unwrap();
                }
            }
            Err(err) => {
                log::error!("Unable to read cache: {err}");
                self.open_file(&PathBuf::from("ask.svg")).unwrap();
            }
        }
        self.set_title(None);

        self.estimate_cut_time();

        let service = ctx.address();

        let mouse_down = Arc::new(AtomicBool::new(false));
        let drag = Arc::new(AtomicBool::new(false));
        let selected_mode = Arc::new(AtomicMode::new(Mode::default()));
        let notification_service = self.notification_service.clone();
        self.handle
            .upgrade_in_event_loop(move |handle| {
                let mode = selected_mode.clone();
                handle.on_draw_line(move || {
                    mode.store(Mode::Line, Ordering::SeqCst);
                });
                let mode = selected_mode.clone();
                handle.on_select(move || {
                    mode.store(Mode::Select, Ordering::SeqCst);
                });

                let s = service.clone();
                handle.on_generate_gcode(move || {
                    s.do_send(GenerateGcode);
                });
                let s = service.clone();
                handle.on_select_all(move || {
                    s.do_send(SelectAll);
                });
                let s = service.clone();
                handle.on_simulate(move || {
                    s.do_send(Simulate);
                });
                let s = service.clone();
                handle.on_clear(move || {
                    s.do_send(Clear);
                });

                let s = service.clone();
                let d = drag.clone();
                handle.on_start_drag(move |x, y| {
                    s.do_send(Drag(x, y));
                    d.store(true, Ordering::SeqCst);
                });
                let s = service.clone();
                let d = drag.clone();
                handle.on_end_drag(move |_x, _y| {
                    s.do_send(EndDrag);
                    d.store(false, Ordering::SeqCst);
                });

                let s = service.clone();
                handle.on_change_scale(move |delta| {
                    if delta > 0. {
                        s.do_send(IncreaseScale(1.25));
                    } else {
                        s.do_send(DecreaseScale(1.25));
                    }
                });

                let s = service.clone();
                handle
                    .on_change_order(move |order| s.do_send(ChangeOrderOfSelected(order as usize)));

                let s = service.clone();
                handle.on_arrange_selected(move || s.do_send(ArrangeSelected));

                let s = service.clone();
                handle.on_enable_display_order(move || s.do_send(SetDisplayOrder(true)));

                let s = service.clone();
                handle.on_disable_display_order(move || s.do_send(SetDisplayOrder(false)));

                let s = service.clone();
                handle.on_set_width(move |w| s.do_send(SetWidth(w as u32)));

                let s = service.clone();
                handle.on_set_height(move |h| s.do_send(SetHeight(h as u32)));

                let s = service.clone();
                handle.on_align_selected(move |h| s.do_send(AlignSelected(h)));

                let s = service.clone();
                handle.on_history_forward(move || s.do_send(HistoryForward));
                let s = service.clone();
                handle.on_history_back(move || s.do_send(HistoryBack));

                let s = service.clone();
                handle.on_zoom_in(move || s.do_send(IncreaseScale(2.)));
                let s = service.clone();
                handle.on_zoom_out(move || s.do_send(DecreaseScale(2.)));

                let s = service.clone();
                handle.on_connect(move || s.do_send(SearchForDevice));

                let m_d = mouse_down.clone();
                let mode = selected_mode.clone();
                let d = drag.clone();

                let mut last_hover_time = std::time::Instant::now();
                let s = service.clone();
                handle.on_hover(move |event, x, y| {
                    let control_pressed = event.modifiers.control;
                    let mouse_down = m_d.load(Ordering::SeqCst);
                    let mode = mode.load(Ordering::SeqCst);
                    let drag = d.load(Ordering::SeqCst);
                    if last_hover_time.elapsed() < std::time::Duration::from_millis(1000 / 60) {
                        return;
                    }
                    last_hover_time = std::time::Instant::now();
                    match (drag, mouse_down, mode) {
                        (false, true, Mode::Line) => s.do_send(DragRuler(x, y)),
                        (true, _, _) => s.do_send(Drag(x, y)),
                        (_, true, _) => s.do_send(Hover {
                            x,
                            y,
                            control_pressed,
                        }),
                        _ => (),
                    };
                });

                let m_d = mouse_down.clone();
                handle.on_mouse_down(move |_x, _y| {
                    m_d.store(true, Ordering::SeqCst);
                });

                let m_d = mouse_down.clone();
                let mode = selected_mode.clone();
                let s = service.clone();
                let h = handle.as_weak();
                handle.on_mouse_up(move |x, y| {
                    m_d.store(false, Ordering::SeqCst);
                    let mode = mode.load(Ordering::SeqCst);
                    match mode {
                        Mode::Line => s.do_send(EndDragRuler(x, y)),
                        Mode::Select => (),
                    }
                    let s = s.clone();
                    h.upgrade_in_event_loop(move |handle| {
                        if let Mode::Select = mode {
                            if handle.get_control_pressed() {
                                s.do_send(AddFigureToSelection(x, y));
                            } else {
                                s.do_send(SelectFigure(x, y));
                            }
                        }
                        s.do_send(EndResize);
                        s.do_send(EndDragSelectionBox);
                        s.do_send(EndMoveSelected);
                    })
                    .ok_or_notify(&notification_service);
                });

                let s = service.clone();
                handle.on_open_file(move || s.do_send(Open));

                let s = service.clone();
                handle.on_save(move || s.do_send(Save));

                let s = service.clone();
                handle.on_set_selected_width(move |w| {
                    s.do_send(SetSelectedWidth(w));
                });

                let s = service.clone();
                handle.on_set_selected_height(move |h| {
                    s.do_send(SetSelectedHeight(h));
                });

                let s = service.clone();
                handle.on_toggle_position(move || {
                    s.do_send(EnableDeviceLocation);
                });

                let s = service.clone();
                handle.on_device_move_up(move || s.do_send(DeviceMove(Movement::Up)));
                let s = service.clone();
                handle.on_device_move_down(move || s.do_send(DeviceMove(Movement::Down)));
                let s = service.clone();
                handle.on_device_move_left(move || s.do_send(DeviceMove(Movement::Left)));
                let s = service.clone();
                handle.on_device_move_right(move || s.do_send(DeviceMove(Movement::Right)));
                let s = service.clone();
                handle.on_device_execute(move || s.do_send(DeviceExecute));
                let s = service.clone();
                handle.on_device_stop(move || s.do_send(DeviceStop));
                let s = service.clone();
                handle.on_device_pause(move || s.do_send(DevicePause));
                let s = service.clone();
                handle.on_device_resume(move || s.do_send(DeviceResume));
                let s = service.clone();
                let (mut last_width, mut last_height) = (0., 0.);
                handle.on_redraw(move |w, h, ww, wh| {
                    if w != last_width || h != last_height {
                        last_width = w;
                        last_height = h;
                        s.do_send(Redraw(Some((w, h))));
                        s.do_send(SetWindowSize(ww, wh));
                    }
                });
            })
            .ok_or_notify(&self.notification_service);
    }

    fn stopped(&mut self, _: &mut Self::Context) {
        if let Err(err) = self.write_cached_settings() {
            log::error!("Unable to write cache: {err}");
        }
    }
}

impl Handler<ChangeScale> for ApplicationService {
    type Result = ();

    fn handle(&mut self, ChangeScale(scale): ChangeScale, ctx: &mut Self::Context) {
        self.scale = scale;
        ctx.address().do_send(Redraw(None));
    }
}

impl Handler<IncreaseScale> for ApplicationService {
    type Result = ();

    fn handle(&mut self, IncreaseScale(factor): IncreaseScale, ctx: &mut Self::Context) {
        let new_scale = self.scale * factor;
        let scale = self.scale;
        let pause = std::time::Duration::from_millis(13);
        for i in 1..5 {
            self.scale = scale * (1. + i as f32 / (5. / (factor - 1.)));
            let instant = std::time::Instant::now();
            self.handle(Redraw(None), ctx);
            if instant.elapsed() < pause {
                std::thread::sleep(pause - instant.elapsed());
            }
        }
        self.scale = new_scale;
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<DecreaseScale> for ApplicationService {
    type Result = ();

    fn handle(&mut self, DecreaseScale(factor): DecreaseScale, ctx: &mut Self::Context) {
        if (self.viewport_width as f32 / (self.scale / factor)) >= (self.ppmm * self.width as f32)
            && (self.viewport_height as f32 / (self.scale / factor))
                >= (self.ppmm * self.height as f32)
        {
            self.scale = (self.viewport_width as f32 / (self.ppmm * self.width as f32))
                .min(self.viewport_height as f32 / (self.ppmm * self.height as f32));
            ctx.address().do_send(Redraw(None));
            return;
        }
        let new_scale = self.scale / factor;
        let scale = self.scale;
        let pause = std::time::Duration::from_millis(13);
        for i in 1..5 {
            self.scale = scale / (1. + i as f32 / (5. / (factor - 1.)));
            let instant = std::time::Instant::now();
            self.handle(Redraw(None), ctx);
            if instant.elapsed() < pause {
                std::thread::sleep(pause - instant.elapsed());
            }
        }
        self.scale = new_scale;
        ctx.address().do_send(Redraw(None));
    }
}

impl Handler<AddFigure> for ApplicationService {
    type Result = ();

    fn handle(&mut self, AddFigure(figure): AddFigure, ctx: &mut Self::Context) {
        self.figures.push(Arc::new(figure));
        ctx.address().do_send(Redraw(None));
    }
}

impl Handler<AddFigures> for ApplicationService {
    type Result = ();

    fn handle(&mut self, AddFigures(figures): AddFigures, ctx: &mut Self::Context) {
        self.add_figures(figures);
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<Redraw> for ApplicationService {
    type Result = ();

    fn handle(&mut self, Redraw(wh): Redraw, _: &mut Self::Context) {
        let notification_service = self.notification_service.clone();
        let scale = self.scale;
        let handle = self.handle.clone();
        if let Some((w, h)) = wh {
            self.viewport_width = w as u32 - 80;
            self.viewport_height = h as u32 - 80;
            self.ruler_buffer =
                SharedPixelBuffer::<Rgba8Pixel>::new(self.viewport_width, self.viewport_height);
        }
        self.canvas_buffer =
            SharedPixelBuffer::<Rgba8Pixel>::new(self.viewport_width, self.viewport_height);
        let width = self.canvas_buffer.width();
        let height = self.canvas_buffer.height();
        let res =
            tiny_skia::PixmapMut::from_bytes(self.canvas_buffer.make_mut_bytes(), width, height)
                .context("Unable to create pixmap")
                .ok_or_notify(&notification_service);
        let pixmap = match res {
            Some(r) => r,
            None => return,
        };
        let transform = Transform::from_scale(scale, scale).pre_translate(-self.x, -self.y);

        let pixmap = Self::render_figures(
            pixmap,
            transform,
            self.width,
            self.height,
            self.ppmm,
            self.scale,
            &self.figures,
        );

        if self.display_order {
            Self::render_order(
                pixmap,
                &mut self.cache,
                self.x,
                self.y,
                self.scale,
                &self.figures,
            );
        }

        let canvas_buf = self.canvas_buffer.clone();

        let selection_buf;
        if let Some(f) = self.selected_figures.clone() {
            let f = f.iter().collect::<Vec<_>>();
            selection_buf = Some(draw_selected_figures(
                f.iter().map(AsRef::as_ref),
                transform,
                self.viewport_width,
                self.viewport_height,
                self.scale,
            ));
        } else {
            selection_buf = None;
        }
        let selection_buf = if let Some(Action::SelectionBox(from, Some(to))) = self.action {
            let stroke = tiny_skia::Stroke {
                width: 1. / scale,
                dash: tiny_skia::StrokeDash::new(vec![5.0 / scale, 5.0 / scale], 0.0),
                ..Default::default()
            };
            let transform = Transform::from_scale(scale, scale);
            Some(draw_selection_box(
                from,
                to,
                transform,
                &stroke,
                selection_buf.unwrap_or(SharedPixelBuffer::<Rgba8Pixel>::new(
                    self.viewport_width,
                    self.viewport_height,
                )),
            ))
        } else if selection_buf.is_none() {
            Some(SharedPixelBuffer::<Rgba8Pixel>::new(
                self.viewport_width,
                self.viewport_height,
            ))
        } else {
            selection_buf
        };
        handle
            .upgrade_in_event_loop(move |handle| {
                let image = Image::from_rgba8_premultiplied(canvas_buf);
                handle.set_canvas(image);
                if let Some(buf) = selection_buf {
                    let image = Image::from_rgba8_premultiplied(buf);
                    handle.set_selection(image);
                }
            })
            .ok_or_notify(&notification_service);

        let mut rulers_buffer =
            SharedPixelBuffer::<Rgba8Pixel>::new(self.viewport_width + RULER_WIDTH, RULER_WIDTH);
        draw_rulers_horizontal(
            &mut rulers_buffer,
            self.viewport_width,
            scale,
            self.ppmm,
            &mut self.cache,
            self.x,
            self.viewport_width,
        )
        .ok_or_notify(&notification_service);

        let mut vertical_ruler_buffer =
            SharedPixelBuffer::<Rgba8Pixel>::new(RULER_WIDTH, self.viewport_height + RULER_WIDTH);
        draw_rulers_vertical(
            &mut vertical_ruler_buffer,
            self.viewport_height,
            scale,
            self.ppmm,
            &mut self.cache,
            self.y,
            self.viewport_height,
        )
        .ok_or_notify(&notification_service);

        handle
            .upgrade_in_event_loop(move |handle| {
                let image = Image::from_rgba8_premultiplied(rulers_buffer);
                handle.set_horizontal_ruler(image);
                let image = Image::from_rgba8_premultiplied(vertical_ruler_buffer);
                handle.set_vertical_ruler(image);
            })
            .ok_or_notify(&notification_service);

        if let Some((status, trace)) = self.device_status.as_ref() {
            let mut buf =
                SharedPixelBuffer::<Rgba8Pixel>::new(self.viewport_width, self.viewport_height);
            draw_device_position(&mut buf, status, trace, transform);
            handle
                .upgrade_in_event_loop(move |handle| {
                    let image = Image::from_rgba8_premultiplied(buf);
                    handle.set_device_position(image);
                })
                .ok_or_notify(&notification_service);
        }
    }
}

impl Handler<RedrawDevice> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: RedrawDevice, _: &mut Self::Context) {
        if let Some((status, trace)) = self.device_status.as_ref() {
            let transform =
                Transform::from_scale(self.scale, self.scale).pre_translate(-self.x, -self.y);
            let mut buf =
                SharedPixelBuffer::<Rgba8Pixel>::new(self.viewport_width, self.viewport_height);
            draw_device_position(&mut buf, status, trace, transform);
            self.handle
                .upgrade_in_event_loop(move |handle| {
                    let image = Image::from_rgba8_premultiplied(buf);
                    handle.set_device_position(image);
                })
                .ok_or_notify(&self.notification_service);
        }
    }
}

impl Handler<SelectFigure> for ApplicationService {
    type Result = ();

    fn handle(&mut self, SelectFigure(x, y): SelectFigure, _: &mut Self::Context) {
        if self.action.as_ref().is_some_and(Action::is_active) {
            return;
        }
        let scale = self.scale;
        let x = self.x + x / scale;
        let y = self.y + y / scale;
        let mut figures = self
            .figures
            .clone()
            .into_iter()
            .enumerate()
            .collect::<Vec<_>>();
        figures.sort_by(|(_, a), (_, b)| {
            a.area()
                .partial_cmp(&b.area())
                .unwrap_or(std::cmp::Ordering::Equal)
        });
        let figure = figures
            .iter()
            .map(|(i, f)| (*i, f.bounds(), f))
            .find(|(_, f, _)| x > f.left() && x < f.right() && y > f.top() && y < f.bottom());
        let res = match figure {
            Some((i, _, f)) => {
                let transform =
                    Transform::from_scale(self.scale, self.scale).pre_translate(-self.x, -self.y);
                let bounds = f.bounds();
                let figures = vec![f.clone()];
                self.selected_figures = NonEmpty::from_vec(figures.clone());
                Some((
                    i,
                    draw_selected_figures(
                        figures.iter().map(AsRef::as_ref),
                        transform,
                        self.viewport_width,
                        self.viewport_height,
                        self.scale,
                    ),
                    (bounds.right() - bounds.left()) / self.ppmm,
                    (bounds.bottom() - bounds.top()) / self.ppmm,
                ))
            }
            None => {
                self.selected_figures = None;
                None
            }
        };
        let width = self.viewport_width;
        let height = self.viewport_height;
        let handle = self.handle.clone();
        handle
            .upgrade_in_event_loop(move |handle| {
                if let Some((i, buf, w, h)) = res {
                    let image = Image::from_rgba8_premultiplied(buf);
                    handle.set_selection(image);
                    handle.set_selected_order((i + 1).to_string().into());
                    handle.set_selected_width(format!("{w:.2}").into());
                    handle.set_selected_height(format!("{h:.2}").into());
                } else {
                    let image = Image::from_rgba8_premultiplied(
                        SharedPixelBuffer::<Rgba8Pixel>::new(width, height),
                    );
                    handle.set_selection(image);
                    handle.set_selected_order(0.to_string().into());
                    handle.set_selected_width("0.00".into());
                    handle.set_selected_height("0.00".into());
                }
            })
            .ok_or_notify(&self.notification_service);
    }
}

impl Handler<AddFigureToSelection> for ApplicationService {
    type Result = ();

    fn handle(&mut self, AddFigureToSelection(x, y): AddFigureToSelection, _: &mut Self::Context) {
        if self.action.as_ref().is_some_and(Action::is_active) {
            return;
        }
        let scale = self.scale;
        let x = self.x + x / scale;
        let y = self.y + y / scale;
        let mut figures = self
            .figures
            .clone()
            .into_iter()
            .enumerate()
            .collect::<Vec<_>>();
        figures.sort_by(|(_, a), (_, b)| {
            a.area()
                .partial_cmp(&b.area())
                .unwrap_or(std::cmp::Ordering::Equal)
        });
        let figure = figures
            .iter()
            .map(|(i, f)| (i, f, f.bounds()))
            .find(|(_, _, f)| x > f.left() && x < f.right() && y > f.top() && y < f.bottom());
        let bounds =
            figure.map(|(_, _, b)| (b.left() - 3., b.top() - 3., b.right() + 3., b.bottom() + 3.));
        let pixel_buffer;
        let transform =
            Transform::from_scale(self.scale, self.scale).pre_translate(-self.x, -self.y);
        if let Some((_i, figure, _)) = figure {
            if let Some(selected_figures) = &mut self.selected_figures {
                let mut selected_figures = selected_figures.clone().into_iter().collect::<Vec<_>>();
                let index = selected_figures
                    .iter()
                    .enumerate()
                    .find(|(_, f)| *f == figure)
                    .map(|(i, _)| i);
                if let Some(i) = index {
                    selected_figures.remove(i);
                } else {
                    selected_figures.push(figure.clone());
                }
                pixel_buffer = Some(draw_selected_figures(
                    selected_figures.iter().map(AsRef::as_ref),
                    transform,
                    self.viewport_width,
                    self.viewport_height,
                    self.scale,
                ));
                self.selected_figures = NonEmpty::from_vec(selected_figures);
            } else {
                let s = vec![figure.clone()];
                self.selected_figures = NonEmpty::from_vec(s.clone());
                pixel_buffer = Some(draw_selected_figures(
                    s.iter().map(AsRef::as_ref),
                    transform,
                    self.viewport_width,
                    self.viewport_height,
                    self.scale,
                ));
            }
        } else {
            pixel_buffer = None;
            self.selected_figures = None;
        }

        let buf = pixel_buffer;

        let wh = match &self.selection_bounds() {
            Some(bounds) => Some((
                (bounds.right() - bounds.left()) / self.ppmm,
                (bounds.bottom() - bounds.top()) / self.ppmm,
            )),
            None => None,
        };

        self.handle
            .upgrade_in_event_loop(move |handle| {
                if let Some(buf) = buf {
                    let image = Image::from_rgba8_premultiplied(buf);
                    handle.set_selection(image);
                }
                if let Some((left, top, right, bottom)) = bounds {
                    handle.set_selected(Selection {
                        left,
                        top,
                        right,
                        bottom,
                    });
                    if let Some((w, h)) = wh {
                        handle.set_selected_width(format!("{w:.2}").into());
                        handle.set_selected_height(format!("{h:.2}").into());
                    }
                } else {
                    handle.set_selected(Selection::default());
                }
            })
            .ok_or_notify(&self.notification_service);
    }
}

impl Handler<GenerateGcode> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: GenerateGcode, _: &mut Self::Context) {
        let file =
            std::fs::File::create("result.ngc").ok_or_notify(&self.notification_service);
        if let Some(mut file) = file {
            write_gcode(self.figures.iter().cloned(), &mut file)
                .ok_or_notify(&self.notification_service);
        }
    }
}

impl Handler<DragRuler> for ApplicationService {
    type Result = ();

    fn handle(&mut self, DragRuler(x, y): DragRuler, _: &mut Self::Context) {
        let notification_service = self.notification_service.clone();
        match self.ruler {
            Some((x1, y1)) => {
                let scale = self.scale;
                let mut pixel_buffer = self.ruler_buffer.clone();
                let handle = self.handle.clone();
                let mut line = tiny_skia::PathBuilder::new();
                line.move_to(x1 / scale, y1 / scale);
                line.line_to(x / scale, y / scale);
                let line = line.finish().unwrap();
                let width = pixel_buffer.width();
                let height = pixel_buffer.height();
                let mut pixmap =
                    tiny_skia::PixmapMut::from_bytes(pixel_buffer.make_mut_bytes(), width, height)
                        .unwrap();
                pixmap.stroke_path(
                    &line,
                    &Paint {
                        anti_alias: false,
                        ..Default::default()
                    },
                    &tiny_skia::Stroke {
                        width: 1. / scale,
                        ..Default::default()
                    },
                    Transform::from_scale(scale, scale),
                    None,
                );
                let width = x.max(x1) - x.min(x1);
                let height = y.max(y1) - y.min(y1);
                let len = (width.powi(2) + height.powi(2)).sqrt() / scale / self.ppmm;
                let mut rc = self.cache.render_context(&mut pixmap);
                let text = rc
                    .text()
                    .new_text_layout(format!("{:.2}мм", len))
                    .font(piet::FontFamily::MONOSPACE, 10.)
                    .build()
                    .unwrap();
                let x = x as f64 + 10.;
                let y = y as f64 - 10.;
                rc.draw_text(&text, piet::kurbo::Point { x, y });
                let buf = pixel_buffer;
                handle
                    .upgrade_in_event_loop(move |handle| {
                        let image = Image::from_rgba8_premultiplied(buf);
                        handle.set_ruler(image);
                    })
                    .ok_or_notify(&notification_service);
            }
            None => {
                self.ruler = Some((x, y));
            }
        }
    }
}

impl Handler<Simulate> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: Simulate, _: &mut Self::Context) {
        let handle = self.handle.clone();
        let notification_service = self.notification_service.clone();
        let scale = self.scale;
        let mut pixel_buffer =
            SharedPixelBuffer::<Rgba8Pixel>::new(self.viewport_width, self.viewport_height);
        let width = pixel_buffer.width();
        let height = pixel_buffer.height();
        let mut pixmap =
            tiny_skia::PixmapMut::from_bytes(pixel_buffer.make_mut_bytes(), width, height)
                .unwrap()
                .to_owned();

        let mut last_pos = (0., 0.);
        for f in self.figures.iter() {
            let mut path = tiny_skia::PathBuilder::new();
            let mut pos = (0., 0.);
            let mut initial = None;
            let transform = Transform::from_scale(scale, scale).pre_translate(-self.x, -self.y);
            for segment in f.path.segments() {
                let points = match segment {
                    PathSegment::MoveTo(Point { x, y }) => {
                        initial = Some((x, y));
                        pos.0 = x;
                        pos.1 = y;
                        vec![(x, y, false)]
                    }
                    PathSegment::LineTo(Point { x, y }) => {
                        initial = initial.or(Some((pos.0, pos.1)));
                        pos.0 = x;
                        pos.1 = y;
                        vec![(x, y, true)]
                    }
                    PathSegment::QuadTo(a, b) => {
                        let res = interpolate_bezier_quad((pos.into(), a, b), BEZIER_STEPS);
                        pos.0 = b.x;
                        pos.1 = b.y;
                        res.into_iter().map(|Point { x, y }| (x, y, true)).collect()
                    }
                    PathSegment::CubicTo(a, b, c) => {
                        let res = interpolate_bezier_cubic((pos.into(), a, b, c), BEZIER_STEPS);
                        pos.0 = c.x;
                        pos.1 = c.y;
                        res.into_iter().map(|Point { x, y }| (x, y, true)).collect()
                    }
                    PathSegment::Close => {
                        if let Some((x, y)) = initial {
                            pos.0 = x;
                            pos.1 = y;
                            vec![(x, y, true)]
                        } else {
                            vec![]
                        }
                    }
                };
                let mut len;
                for (x, y, z) in points {
                    if z {
                        path.line_to(x, y);
                        len = ((x - last_pos.0).abs().powi(2) + (y - last_pos.1).abs().powi(2))
                            .sqrt()
                            / self.ppmm;
                    } else {
                        path.move_to(x, y);
                        last_pos = (x, y);
                        continue;
                    }
                    let p = path.finish().unwrap();
                    path = tiny_skia::PathBuilder::new();
                    path.move_to(x, y);
                    last_pos = (x, y);
                    pixmap.stroke_path(
                        &p,
                        &Paint {
                            anti_alias: false,
                            shader: tiny_skia::Shader::SolidColor(tiny_skia::Color::from_rgba8(
                                0, 0, 255, 255,
                            )),
                            ..Default::default()
                        },
                        &tiny_skia::Stroke {
                            width: 2. / scale,
                            miter_limit: 0.1,
                            ..Default::default()
                        },
                        transform,
                        None,
                    );
                    if len > 0. {
                        std::thread::sleep(std::time::Duration::from_micros(
                            (1. / (self.cut_rate / len) * 1000. * 1000.) as u64,
                        ));
                    }
                    let pixel_buffer =
                        SharedPixelBuffer::clone_from_slice(pixmap.as_ref().data(), width, height);
                    handle
                        .upgrade_in_event_loop(move |handle| {
                            let image = Image::from_rgba8_premultiplied(pixel_buffer);
                            handle.set_simulation(image);
                        })
                        .ok_or_notify(&notification_service);
                }
            }
        }
        for _ in 0..100 {
            for pixel in pixmap.pixels_mut() {
                *pixel = tiny_skia::PremultipliedColorU8::from_rgba(
                    (pixel.red() as f32 / 1.01) as u8,
                    (pixel.green() as f32 / 1.01) as u8,
                    (pixel.blue() as f32 / 1.01) as u8,
                    (pixel.alpha() as f32 / 1.01) as u8,
                )
                .unwrap();
            }
            let pixel_buffer =
                SharedPixelBuffer::clone_from_slice(pixmap.as_ref().data(), width, height);
            handle
                .upgrade_in_event_loop(move |handle| {
                    let image = Image::from_rgba8_premultiplied(pixel_buffer);
                    handle.set_simulation(image);
                })
                .ok_or_notify(&notification_service);
            std::thread::sleep(std::time::Duration::from_millis(26));
        }
        pixmap.fill(tiny_skia::Color::TRANSPARENT);
        handle
            .upgrade_in_event_loop(move |handle| {
                let image = Image::from_rgba8_premultiplied(pixel_buffer);
                handle.set_simulation(image);
            })
            .ok_or_notify(&notification_service);
    }
}

impl Handler<Clear> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: Clear, _: &mut Self::Context) {}
}

impl Handler<SelectAll> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: SelectAll, ctx: &mut Self::Context) {
        self.selected_figures = NonEmpty::from_vec(self.figures.clone());
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<EndDragRuler> for ApplicationService {
    type Result = ();

    fn handle(&mut self, EndDragRuler(_, _): EndDragRuler, _: &mut Self::Context) {
        self.ruler = None;
        self.ruler_buffer =
            SharedPixelBuffer::<Rgba8Pixel>::new(self.viewport_width, self.viewport_height);
        let buf = self.ruler_buffer.clone();
        self.handle
            .upgrade_in_event_loop(move |handle| {
                let image = Image::from_rgba8_premultiplied(buf);
                handle.set_ruler(image);
            })
            .ok_or_notify(&self.notification_service);
    }
}

impl Handler<Drag> for ApplicationService {
    type Result = ();

    fn handle(&mut self, Drag(x, y): Drag, ctx: &mut Self::Context) {
        let x = x / self.scale;
        let y = y / self.scale;
        let (dx, dy) = match self.last_drag {
            Some((x1, y1)) => {
                self.last_drag = Some((x, y));
                (x1 - x, y1 - y)
            }
            None => {
                self.last_drag = Some((x, y));
                return;
            }
        };
        self.x = (self.x + dx).max(0.);
        self.y = (self.y + dy).max(0.);
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<EndDrag> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: EndDrag, _: &mut Self::Context) {
        self.last_drag = None;
    }
}

impl Handler<Resize> for ApplicationService {
    type Result = ();

    fn handle(&mut self, Resize(x, y): Resize, ctx: &mut Self::Context) {
        let x = self.x + x / self.scale;
        let y = self.y + y / self.scale;
        if let Some(Action::Resize(control)) = &self.action {
            let bounds = match self.selection_bounds() {
                Some(b) => b,
                None => return,
            };

            let resize_point = control.point_of(bounds);

            let mut resize_width = (resize_point.x - x).abs();
            let mut resize_height = (resize_point.y - y).abs();
            if resize_width > self.width as f32 * self.ppmm {
                resize_width = self.height as f32 * self.ppmm;
            }
            if resize_height > self.height as f32 * self.ppmm {
                resize_height = self.height as f32 * self.ppmm;
            }

            let width = bounds.right() - bounds.left();
            let height = bounds.bottom() - bounds.top();
            let ratio = (resize_width / width).min(resize_height / height);

            self.resize_selected(control.clone(), ratio)
                .ok_or_notify(&self.notification_service);
        }
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<EndResize> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: EndResize, _: &mut Context<Self>) {
        if let Some(Action::Resize(..)) = self.action {
            self.push_history_from_buf();
            self.action = None;
        }
    }
}

impl Handler<MoveSelected> for ApplicationService {
    type Result = ();

    fn handle(&mut self, MoveSelected(x, y): MoveSelected, ctx: &mut Self::Context) {
        let x = self.x + x / self.scale;
        let y = self.y + y / self.scale;
        let (initial, last_move) = match self.action {
            Some(Action::Move(i, l)) => {
                self.action = Some(Action::Move(i, Some((x, y).into())));
                (i, l.unwrap_or(i))
            }
            None => {
                self.action = Some(Action::Move((x, y).into(), None));
                return;
            }
            _ => return,
        };
        let bounds = match self.selection_bounds() {
            Some(b) => b,
            None => return,
        };

        let delta_x = x - last_move.x;
        let delta_y = y - last_move.y;

        let overflow_x = bounds.left() + delta_x <= 0.
            || bounds.right() + delta_x >= self.width as f32 * self.ppmm;
        let overflow_y = bounds.top() + delta_y <= 0.
            || bounds.bottom() + delta_y >= DEFAULT_WORKSPACE_HEIGHT as f32 * self.ppmm;

        self.action = Some(Action::Move(
            initial,
            Some(
                match (overflow_x, overflow_y) {
                    (true, true) => (last_move.x, last_move.y),
                    (true, false) => (last_move.x, y),
                    (false, true) => (x, last_move.y),
                    (false, false) => (x, y),
                }
                .into(),
            ),
        ));
        self.move_selected((delta_x, delta_y));
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<EndMoveSelected> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: EndMoveSelected, _: &mut Self::Context) -> Self::Result {
        if let Some(Action::Move(..) | Action::Rotate { .. }) = self.action {
            self.push_history_from_buf();
            self.action = None;
        }
    }
}

impl Handler<ChangeOrderOfSelected> for ApplicationService {
    type Result = ();

    fn handle(
        &mut self,
        ChangeOrderOfSelected(order): ChangeOrderOfSelected,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        let selected = match self.selected_figures.as_ref().map(|x| x.first()).cloned() {
            Some(fig) => fig,
            None => {
                self.notification_service.do_send(DisplayNotification(
                    None,
                    "No figure selected".to_string(),
                    NotificationType::Warn,
                ));
                return;
            }
        };
        if order >= self.figures.len() {
            log::warn!("Order must be less than figures len");
            return;
        }
        let index = self
            .figures
            .iter()
            .enumerate()
            .find(|(_, f)| **f == selected)
            .map(|(i, _)| i);
        let index = match index {
            Some(i) => i,
            None => {
                log::warn!("Current index not found");
                return;
            }
        };
        let elem = self.figures.remove(order);
        self.figures.insert(order, selected.clone());
        self.figures.remove(index);
        self.figures.insert(index, elem);
        self.handle(Redraw(None), ctx);
        self.estimate_cut_time();
    }
}

impl Handler<ArrangeSelected> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: ArrangeSelected, ctx: &mut Self::Context) {
        let figures = match &mut self.selected_figures {
            Some(figures) => figures,
            None => {
                self.figures = sort_by_optimal_path(self.figures.clone(), (0., 0.).into());
                sort_nested(&mut self.figures);
                self.handle(Redraw(None), ctx);
                self.estimate_cut_time();
                return;
            }
        };
        if figures.len() < 2 {
            return;
        }
        let mut figures: Vec<_> = figures.clone().into_iter().collect();
        figures.sort_by(|a, b| {
            a.area()
                .partial_cmp(&b.area())
                .unwrap_or(std::cmp::Ordering::Equal)
        });
        for f in figures.iter().rev() {
            let index = self
                .figures
                .iter()
                .enumerate()
                .find(|(_, figure)| *figure == f)
                .map(|(i, _)| i);
            let index = match index {
                Some(i) => i,
                None => {
                    log::warn!("Current index not found");
                    continue;
                }
            };
            let elem = self.figures.remove(index);
            self.figures.insert(0, elem);
        }
        self.selected_figures = NonEmpty::from_vec(figures);
        self.handle(Redraw(None), ctx);
        self.estimate_cut_time();
    }
}

impl Handler<SetDisplayOrder> for ApplicationService {
    type Result = ();

    fn handle(&mut self, SetDisplayOrder(display_order): SetDisplayOrder, ctx: &mut Self::Context) {
        self.display_order = display_order;
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<DragSelectionBox> for ApplicationService {
    type Result = ();

    fn handle(&mut self, DragSelectionBox(x, y): DragSelectionBox, ctx: &mut Self::Context) {
        let x = x / self.scale;
        let y = y / self.scale;
        let point = (x, y).into();
        match self.action {
            Some(Action::SelectionBox(from, Some(_))) => {
                self.action = Some(Action::SelectionBox(from, Some(point)));
                self.handle(Redraw(None), ctx);
            }
            Some(Action::SelectionBox(from, None)) => {
                self.action = Some(Action::SelectionBox(from, Some(point)));
            }
            None => {
                self.action = Some(Action::SelectionBox(point, None));
            }
            _ => (),
        }
    }
}

impl Handler<EndDragSelectionBox> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: EndDragSelectionBox, ctx: &mut Self::Context) {
        let (from, to) = match self.action {
            Some(Action::SelectionBox(from, Some(to))) => (from, to),
            Some(Action::SelectionBox(_, None)) => {
                self.action = None;
                return;
            }
            _ => return,
        };
        let from = Point {
            x: self.x + from.x,
            y: self.y + from.y,
        };
        let to = Point {
            x: self.x + to.x,
            y: self.y + to.y,
        };
        let mut figures = self
            .figures
            .clone()
            .into_iter()
            .enumerate()
            .collect::<Vec<_>>();
        figures.sort_by(|(_, a), (_, b)| {
            a.area()
                .partial_cmp(&b.area())
                .unwrap_or(std::cmp::Ordering::Equal)
        });
        let figures = figures
            .iter()
            .map(|(i, f)| (*i, f.bounds(), f))
            .filter(|(_, f, _)| {
                from.x.min(to.x) < f.left()
                    && from.x.max(to.x) > f.right()
                    && from.y.min(to.y) < f.top()
                    && from.y.max(to.y) > f.bottom()
            })
            .map(|(_, _, f)| f)
            .cloned()
            .collect::<Vec<_>>();
        self.selected_figures = NonEmpty::from_vec(figures);

        let (width, height) = match self.selection_bounds() {
            Some(b) => {
                let width = b.width() / self.ppmm;
                let height = b.height() / self.ppmm;
                (width, height)
            }
            None => (0., 0.),
        };

        self.handle
            .upgrade_in_event_loop(move |handle| {
                handle.set_selected_width(format!("{width:.2}").into());
                handle.set_selected_height(format!("{height:.2}").into());
            })
            .ok_or_notify(&self.notification_service);
        self.action = None;
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<Hover> for ApplicationService {
    type Result = ();

    fn handle(
        &mut self,
        Hover {
            x,
            y,
            control_pressed,
        }: Hover,
        ctx: &mut Self::Context,
    ) {
        if let Some(Action::Resize(_)) = self.action {
            self.handle(Resize(x, y), ctx);
        } else if let Some(Action::Move(_, _)) = self.action {
            self.handle(MoveSelected(x, y), ctx);
        } else if let Some(Action::SelectionBox(..)) = self.action {
            self.handle(DragSelectionBox(x, y), ctx);
        } else if let Some(Action::Rotate { .. }) = self.action {
            self.handle(Rotate(x, y), ctx);
        } else {
            let bounds = match self.selection_bounds() {
                Some(b) => b,
                None => {
                    self.handle(DragSelectionBox(x, y), ctx);
                    return;
                }
            };

            let sx = self.x + x / self.scale;
            let sy = self.y + y / self.scale;

            let (left, right, top, bottom) =
                (bounds.left(), bounds.right(), bounds.top(), bounds.bottom());
            let offset = (FIGURE_CONTROL_WIDTH * 2.) / self.scale;
            let control = match sx {
                x if x > left - offset && x < left + offset => match sy {
                    y if y > top - offset && y < top + offset => Some(FigureControl::TopLeft),
                    y if y > bottom - offset && y < bottom + offset => {
                        Some(FigureControl::BottomLeft)
                    }
                    _ => None,
                },
                x if x > right - offset && x < right + offset => match sy {
                    y if y > top - offset && y < top + offset => Some(FigureControl::TopRight),
                    y if y > bottom - offset && y < bottom + offset => {
                        Some(FigureControl::BottomRight)
                    }
                    _ => None,
                },
                _ => None,
            };
            if let Some(control) = control {
                if control_pressed {
                    let x = left + (right - left) / 2.;
                    let y = top + (bottom - top) / 2.;
                    self.action = Some(Action::Rotate {
                        point: Point { x, y },
                        last_angle: None,
                    });
                } else {
                    self.action = Some(Action::Resize(ResizeControl::new(control.rev())));
                }
                return;
            }

            if sx > left && sx < right && sy > top && sy < bottom {
                self.handle(MoveSelected(x, y), ctx);
            } else {
                self.handle(DragSelectionBox(x, y), ctx);
            }
        }
    }
}

impl Handler<Save> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: Save, ctx: &mut Self::Context) -> Self::Result {
        if let Some(path) = self.file.clone() {
            self.save_file(path).unwrap();
        }
        let path = FileDialog::new()
            .add_filter("Project file", &["json"])
            .add_filter("SVG Image", &["svg"])
            .show_save_single_file()
            .unwrap();
        self.file = path.clone();
        let addr = ctx.address();
        let path = match path {
            Some(path) => path,
            None => return,
        };
        self.save_file(&path).unwrap();
        let filename = path.file_name().unwrap().to_str().unwrap().to_string();
        addr.do_send(SetWindowTitle(Some(filename)));
    }
}

impl Handler<Open> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: Open, ctx: &mut Self::Context) -> Self::Result {
        let path = FileDialog::new()
            .add_filter("Project file", &["json"])
            .add_filter("SVG Image", &["svg"])
            .show_open_single_file()
            .unwrap();
        let path = match path {
            Some(path) => path,
            None => return,
        };
        self.open_file(&path).unwrap();
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<SetWidth> for ApplicationService {
    type Result = ();

    fn handle(&mut self, SetWidth(width): SetWidth, ctx: &mut Self::Context) -> Self::Result {
        if width > 0 {
            self.width = width;
            self.handle(Redraw(None), ctx);
        }
    }
}

impl Handler<SetHeight> for ApplicationService {
    type Result = ();

    fn handle(&mut self, SetHeight(height): SetHeight, ctx: &mut Self::Context) -> Self::Result {
        if height > 0 {
            self.height = height;
            self.handle(Redraw(None), ctx);
        }
    }
}

impl Handler<SetSelectedWidth> for ApplicationService {
    type Result = ();

    fn handle(
        &mut self,
        SetSelectedWidth(width): SetSelectedWidth,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        let bounds = match self.selection_bounds() {
            Some(b) => b,
            None => return,
        };

        let f_width = bounds.width();
        let f_height = bounds.height();
        if width == 0. {
            self.handle
                .upgrade_in_event_loop(move |handle| {
                    handle.set_selected_width(format!("{f_width:.2}").into());
                })
                .ok_or_notify(&self.notification_service);
            return;
        }
        let width = width * self.ppmm;
        let ratio = width / f_width;
        let width_ratio = {
            let w = self.width as f32 * self.ppmm - bounds.left();
            w / f_width
        };
        let height_ratio = {
            let h = self.height as f32 * self.ppmm - bounds.top();
            h / f_height
        };
        let ratio = ratio.min(width_ratio).min(height_ratio);
        self.resize_selected(ResizeControl::new(FigureControl::TopLeft), ratio)
            .ok_or_notify(&self.notification_service);
        self.push_history_from_buf();
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<SetSelectedHeight> for ApplicationService {
    type Result = ();

    fn handle(
        &mut self,
        SetSelectedHeight(height): SetSelectedHeight,
        ctx: &mut Self::Context,
    ) -> Self::Result {
        let height = height * self.ppmm;
        let bounds = match self.selection_bounds() {
            Some(b) => b,
            None => return,
        };

        let f_width = bounds.width();
        let f_height = bounds.height();
        let ratio = height / f_height;
        let width_ratio = {
            let w = self.width as f32 * self.ppmm - bounds.left();
            w / f_width
        };
        let height_ratio = {
            let h = self.height as f32 * self.ppmm - bounds.top();
            h / f_height
        };
        let ratio = ratio.min(width_ratio).min(height_ratio);
        self.resize_selected(ResizeControl::new(FigureControl::TopLeft), ratio)
            .ok_or_notify(&self.notification_service);
        self.push_history_from_buf();
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<AlignSelected> for ApplicationService {
    type Result = ();

    fn handle(&mut self, AlignSelected(alignment): AlignSelected, ctx: &mut Self::Context) {
        let cmp = |a: &f32, b: &f32| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal);
        let bounds = match self.selection_bounds() {
            Some(b) => b,
            None => return,
        };
        let selected_figures = match &mut self.selected_figures {
            Some(f) => f,
            None => return,
        };
        match alignment {
            FigureAlignment::Left => {
                let left = bounds.left();
                let leftmost = selected_figures
                    .iter()
                    .cloned()
                    .min_by(|a, b| cmp(&a.bounds().left(), &b.bounds().left()));

                if let Some(leftmost) = leftmost {
                    self.without_figure(leftmost, |s| {
                        let l = match s.selection_bounds() {
                            Some(b) => b.left(),
                            None => return,
                        };
                        s.move_selected((left - l, 0.));
                        s.push_history_from_buf();
                    });
                }
            }
            FigureAlignment::Right => {
                let right = bounds.right();
                let rightmost = selected_figures
                    .iter()
                    .cloned()
                    .max_by(|a, b| cmp(&a.bounds().right(), &b.bounds().right()));

                if let Some(rightmost) = rightmost {
                    self.without_figure(rightmost, |s| {
                        let r = match s.selection_bounds() {
                            Some(b) => b.right(),
                            None => return,
                        };
                        s.move_selected((right - r, 0.));
                        s.push_history_from_buf();
                    });
                }
            }
            FigureAlignment::Top => {
                let top = bounds.top();
                let topmost = selected_figures
                    .iter()
                    .cloned()
                    .min_by(|a, b| cmp(&a.bounds().top(), &b.bounds().top()));

                if let Some(topmost) = topmost {
                    self.without_figure(topmost, |s| {
                        let t = match s.selection_bounds() {
                            Some(b) => b.top(),
                            None => return,
                        };
                        s.move_selected((0., top - t));
                        s.push_history_from_buf();
                    });
                }
            }
            FigureAlignment::Bottom => {
                let bottom = bounds.bottom();
                let bottommost = selected_figures
                    .iter()
                    .cloned()
                    .max_by(|a, b| cmp(&a.bounds().bottom(), &b.bounds().bottom()));

                if let Some(bottommost) = bottommost {
                    self.without_figure(bottommost, |s| {
                        let b = match s.selection_bounds() {
                            Some(b) => b.bottom(),
                            None => return,
                        };
                        s.move_selected((0., bottom - b));
                        s.push_history_from_buf();
                    });
                }
            }
            FigureAlignment::VerticalCenter => {
                let bounds = selected_figures.first().bounds();
                let center = bounds.top() + (bounds.height()) / 2.;

                self.map_selected(|f| {
                    let bounds = f.bounds();
                    let c = bounds.top() + (bounds.height()) / 2.;
                    let translate_y = c - center;
                    f.clone()
                        .transform(Transform::from_translate(0., -translate_y))
                        .unwrap()
                });
            }
            FigureAlignment::HorizontalCenter => {
                let bounds = selected_figures.first().bounds();
                let center = bounds.left() + bounds.width() / 2.;

                self.map_selected(|f| {
                    let bounds = f.bounds();
                    let c = bounds.left() + bounds.width() / 2.;
                    let translate_x = c - center;
                    f.clone()
                        .transform(Transform::from_translate(-translate_x, 0.))
                        .unwrap()
                });
            }
        }
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<Rotate> for ApplicationService {
    type Result = ();

    fn handle(&mut self, Rotate(x, y): Rotate, ctx: &mut Self::Context) {
        let x = self.x + x / self.scale;
        let y = self.y + y / self.scale;
        let (rotation_point, last_angle) = match &self.action {
            Some(Action::Rotate {
                point,
                last_angle: Some(angle),
            }) => (*point, *angle),
            Some(Action::Rotate { point, .. }) => {
                let point = *point;
                let w = point.x - x;
                let h = point.y - y;

                let mut angle = 90. + (w / h).atan().to_degrees();
                if h < 0. {
                    angle += 180.;
                }
                self.action = Some(Action::Rotate {
                    point,
                    last_angle: Some(angle),
                });
                return;
            }
            _ => return,
        };
        let w = rotation_point.x - x;
        let h = rotation_point.y - y;

        let mut angle = 90. + (w / h).atan().to_degrees();
        if h < 0. {
            angle += 180.;
        }
        self.action = Some(Action::Rotate {
            point: rotation_point,
            last_angle: Some(angle),
        });
        let angle = last_angle - angle;

        self.transform_selected(Transform::from_rotate_at(
            angle,
            rotation_point.x,
            rotation_point.y,
        ));
        self.handle(Redraw(None), ctx);
    }
}

impl Handler<HistoryBack> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: HistoryBack, ctx: &mut Self::Context) {
        let len = self.history.len();
        if self.history_offset + 1 > len {
            return;
        }
        self.history_offset += 1;
        let entry = self.history.get(len - self.history_offset).cloned();
        if let Some(entry) = entry {
            self.apply_history_entry_rev(&entry);
            self.handle(Redraw(None), ctx);
            self.estimate_cut_time();
        }
        let (b, f) = match (self.history.len(), self.history_offset) {
            (0, _) => (false, false),
            (l, o) => (o < l, o > 0),
        };
        let bounds = self.selection_bounds();
        let ppmm = self.ppmm;
        self.handle
            .upgrade_in_event_loop(move |handle| {
                handle.set_history_available_back(b);
                handle.set_history_available_forward(f);
                if let Some(b) = bounds {
                    let (w, h) = (b.width() / ppmm, b.height() / ppmm);
                    handle.set_selected_width(format!("{w:.2}").into());
                    handle.set_selected_height(format!("{h:.2}").into());
                }
            })
            .ok_or_notify(&self.notification_service);
    }
}

impl Handler<HistoryForward> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: HistoryForward, ctx: &mut Self::Context) {
        if self.history_offset == 0 {
            return;
        }
        let entry = self
            .history
            .get(self.history.len() - self.history_offset)
            .cloned();
        if let Some(entry) = entry {
            self.apply_history_entry(&entry);
            self.handle(Redraw(None), ctx);
            self.estimate_cut_time();
        }
        self.history_offset -= 1;
        let (b, f) = match (self.history.len(), self.history_offset) {
            (0, _) => (false, false),
            (l, o) => (o < l, o > 0),
        };
        let bounds = self.selection_bounds();
        let ppmm = self.ppmm;
        self.handle
            .upgrade_in_event_loop(move |handle| {
                handle.set_history_available_back(b);
                handle.set_history_available_forward(f);
                if let Some(b) = bounds {
                    let (w, h) = (b.width() / ppmm, b.height() / ppmm);
                    handle.set_selected_width(format!("{w:.2}").into());
                    handle.set_selected_height(format!("{h:.2}").into());
                }
            })
            .ok_or_notify(&self.notification_service);
    }
}

impl Handler<DeviceExecute> for ApplicationService {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(&mut self, _: DeviceExecute, _: &mut Self::Context) -> Self::Result {
        let upload_options = self.upload_options.clone();
        let notification_service = self.notification_service.clone();
        let figures = self.figures.clone();
        let handle = self.handle.clone();
        Box::pin(
            async move {
                let (addr, port) = match upload_options {
                    Some(o) => (o.addr, o.port),
                    None => return,
                };
                let (tx, rx) = tokio::sync::oneshot::channel();
                handle
                    .upgrade_in_event_loop(|handle| {
                        tx.send(handle.get_rpm()).unwrap();
                    })
                    .ok_or_notify(&notification_service);
                let rpm: f32 = rx.await.unwrap().parse().unwrap();
                let config = tcp_comm::Configuration {
                    rpm,
                    ..tcp_comm::Configuration::default()
                };

                let socket_addr = std::net::SocketAddr::from((addr, port));
                send_command(socket_addr, TcpCommand::SetConfig(config))
                    .await
                    .ok_or_notify(&notification_service);

                let s = notification_service.clone();
                tokio::spawn(async move {
                    let notification_service = s;
                    let mut stream =
                        match TcpStream::connect(&std::net::SocketAddr::from((addr, 8081)))
                            .await
                            .ok_or_notify(&notification_service)
                        {
                            Some(stream) => stream,
                            None => {
                                return;
                            }
                        };
                    stream
                        .set_nodelay(true)
                        .ok_or_notify(&notification_service);

                    write_gcode_async(figures.into_iter(), &mut stream)
                        .await
                        .ok_or_notify(&notification_service);
                });
                tokio::time::sleep(std::time::Duration::from_millis(500)).await;
                let socket_addr = std::net::SocketAddr::from((addr, port));
                send_command(socket_addr, TcpCommand::Execute)
                    .await
                    .ok_or_notify(&notification_service);

                notification_service.do_send(DisplayNotification(
                    None,
                    "Upload successful".to_string(),
                    NotificationType::Success,
                ));
            }
            .into_actor(self),
        )
    }
}

impl Handler<DeviceStop> for ApplicationService {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(&mut self, _: DeviceStop, _: &mut Self::Context) -> Self::Result {
        let notification_service = self.notification_service.clone();
        let upload_options = self.upload_options.clone();
        Box::pin(
            async move {
                let (addr, port) = match upload_options {
                    Some(o) => (o.addr, o.port),
                    None => return,
                };
                let socket_addr = std::net::SocketAddr::from((addr, port));
                send_command(socket_addr, TcpCommand::StopExecution)
                    .await
                    .ok_or_notify(&notification_service);
            }
            .into_actor(self),
        )
    }
}

impl Handler<DevicePause> for ApplicationService {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(&mut self, _: DevicePause, _: &mut Self::Context) -> Self::Result {
        let notification_service = self.notification_service.clone();
        let upload_options = self.upload_options.clone();
        Box::pin(
            async move {
                let (addr, port) = match upload_options {
                    Some(o) => (o.addr, o.port),
                    None => return,
                };
                let socket_addr = std::net::SocketAddr::from((addr, port));
                send_command(socket_addr, TcpCommand::PauseExecution)
                    .await
                    .ok_or_notify(&notification_service);
            }
            .into_actor(self),
        )
    }
}

impl Handler<DeviceResume> for ApplicationService {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(&mut self, _: DeviceResume, _: &mut Self::Context) -> Self::Result {
        let notification_service = self.notification_service.clone();
        let upload_options = self.upload_options.clone();
        Box::pin(
            async move {
                let (addr, port) = match upload_options {
                    Some(o) => (o.addr, o.port),
                    None => return,
                };
                let socket_addr = std::net::SocketAddr::from((addr, port));
                send_command(socket_addr, TcpCommand::ResumeExecution)
                    .await
                    .ok_or_notify(&notification_service);
            }
            .into_actor(self),
        )
    }
}

impl Handler<Connect> for ApplicationService {
    type Result = ();

    fn handle(&mut self, Connect(ip_addr): Connect, ctx: &mut Self::Context) -> Self::Result {
        let notification_service = self.notification_service.clone();
        if let Some(addr) = ip_addr {
            self.upload_options = Some(UploadOptions {
                addr,
                ..Default::default()
            });
        }
        let handle = self.handle.clone();
        let self_addr = ctx.address();
        let (addr, port) = match &self.upload_options {
            Some(o) => (o.addr, o.port),
            None => return,
        };
        tokio::spawn(async move {
            notification_service.do_send(DisplayNotification(
                None,
                format!("Connecting to {}", addr),
                NotificationType::Info,
            ));
            let socket_addr = std::net::SocketAddr::from((addr, port));
            let stream = with_retry(3, Duration::from_millis(200), || async {
                let connect = std::pin::pin!(TcpStream::connect(&socket_addr));
                Ok::<_, anyhow::Error>(
                    with_timeout(Duration::from_millis(10_000), connect)
                        .await
                        .ok_or(anyhow::anyhow!("Timeout reached"))??,
                )
            })
            .await;
            let mut stream = match stream
                .context(format!("Unable to connect to {}", socket_addr))
                .ok_or_notify(&notification_service)
            {
                Some(s) => s,
                None => return,
            };
            notification_service.do_send(DisplayNotification(
                None,
                "Connected".to_string(),
                NotificationType::Success,
            ));
            stream
                .set_nodelay(true)
                .ok_or_notify(&notification_service);

            let msg = format!(
                "{:512}",
                serde_json::to_string(&TcpQuery {
                    command: TcpCommand::GetStatus,
                })
                .unwrap()
            );
            let socket_addr = stream.local_addr().unwrap();
            let udp_stream = UdpSocket::bind((socket_addr.ip(), 8081))
                .await
                .ok_or_notify(&notification_service);
            let udp_stream = match udp_stream {
                Some(s) => s,
                None => return,
            };
            stream
                .write(msg.as_bytes())
                .await
                .ok_or_notify(&notification_service);
            stream
                .flush()
                .await
                .ok_or_notify(&notification_service);
            with_retry(3, Duration::from_millis(250), || async {
                udp_stream.connect((addr, 8081)).await?;
                udp_stream.send_to(&[0], (addr, 8081)).await?;
                udp_stream.readable().await?;
                Ok::<_, anyhow::Error>(())
            })
            .await
            .ok_or_notify(&notification_service);
            println!("socket is readable");
            handle
                .upgrade_in_event_loop(move |handle| {
                    handle.set_connected(true);
                })
                .ok_or_notify(&notification_service);
            loop {
                let mut buf = [0; 9];
                {
                    let fut = std::pin::pin!(udp_stream.recv(&mut buf));
                    if with_timeout(Duration::from_millis(2000), fut).await.is_none() {
                        notification_service.do_send(DisplayNotification(
                            None,
                            "Device disconnected".to_string(),
                            NotificationType::Error,
                        ));
                        self_addr.do_send(SetDisconnected);
                        self_addr.do_send(SearchForDevice);
                        break;
                    }
                };
                let x = i32::from_be_bytes(buf[1..5].try_into().unwrap());
                let y = i32::from_be_bytes(buf[5..9].try_into().unwrap());
                let status = Some(DeviceStatus {
                    x: x as f32,
                    y: y as f32,
                    status: Status::Idle,
                });

                self_addr.do_send(SetDeviceStatus(status));
                self_addr.do_send(RedrawDevice);
            }
        });
    }
}

impl Handler<SetDisconnected> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: SetDisconnected, _: &mut Self::Context) {
        self.device_status = None;
        let buf = SharedPixelBuffer::<Rgba8Pixel>::new(self.viewport_width, self.viewport_height);
        self.handle
            .upgrade_in_event_loop(move |handle| {
                handle.set_connected(false);
                let image = Image::from_rgba8_premultiplied(buf);
                handle.set_device_position(image);
            })
            .ok_or_notify(&self.notification_service);
    }
}

impl Handler<SetCutRate> for ApplicationService {
    type Result = ();

    fn handle(&mut self, SetCutRate(rate): SetCutRate, _: &mut Self::Context) {
        self.cut_rate = rate;
        self.estimate_cut_time();
    }
}

impl Handler<SetWindowTitle> for ApplicationService {
    type Result = ();

    fn handle(&mut self, SetWindowTitle(title): SetWindowTitle, _: &mut Self::Context) {
        self.set_title(title);
    }
}

impl Handler<EnableDeviceLocation> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: EnableDeviceLocation, _: &mut Self::Context) {
        if let Some((status, trace)) = self.device_status.as_ref() {
            let transform =
                Transform::from_scale(self.scale, self.scale).pre_translate(-self.x, -self.y);
            let mut buf =
                SharedPixelBuffer::<Rgba8Pixel>::new(self.viewport_width, self.viewport_height);
            draw_device_position(&mut buf, status, trace, transform);
            self.handle
                .upgrade_in_event_loop(move |handle| {
                    let image = Image::from_rgba8_premultiplied(buf);
                    handle.set_device_position(image);
                })
                .ok_or_notify(&self.notification_service);
        }
    }
}

impl Handler<DisableDeviceLocation> for ApplicationService {
    type Result = ();

    fn handle(&mut self, _: DisableDeviceLocation, _: &mut Self::Context) {}
}

impl Handler<SetDeviceStatus> for ApplicationService {
    type Result = ();

    fn handle(&mut self, SetDeviceStatus(status): SetDeviceStatus, _: &mut Self::Context) {
        let (old_status, trace) = self.device_status.clone().unzip();
        let mut trace = trace.unwrap_or_default();
        if let Some(DeviceStatus { x, y, .. }) = status
            .as_ref()
            .filter(|s| !old_status.is_some_and(|o| o == **s))
        {
            trace.push((*x, *y));
            if trace.len() > TRACE_LEN {
                trace.remove(0);
            }
        }
        self.device_status = status.zip(Some(trace));
    }
}

impl Handler<DeviceMove> for ApplicationService {
    type Result = ();

    fn handle(&mut self, DeviceMove(mov): DeviceMove, _: &mut Self::Context) {
        let notification_service = self.notification_service.clone();
        let (addr, port) = match &self.upload_options {
            Some(o) => (o.addr, o.port),
            None => return,
        };
        tokio::spawn(async move {
            let socket_addr = std::net::SocketAddr::from((addr, port));
            let cmd = match mov {
                Movement::Up => MoveCommand::Y(-6000),
                Movement::Down => MoveCommand::Y(6000),
                Movement::Left => MoveCommand::X(-6000),
                Movement::Right => MoveCommand::X(6000),
            };
            send_command(socket_addr, TcpCommand::RelativeMove(cmd))
                .await
                .ok_or_notify(&notification_service);
        });
    }
}

impl Handler<SaveState> for ApplicationService {
    type Result = ResponseActFuture<Self, ()>;
    fn handle(&mut self, _: SaveState, _: &mut Self::Context) -> Self::Result {
        let state = ApplicationState {
            file: self.file.clone(),
            window_width: self.window_width,
            window_height: self.window_height,
        };
        Box::pin(
            async move {
                let res = serde_json::to_string(&state).unwrap();
                tokio::fs::write(".state", res).await.unwrap();
            }
            .into_actor(self),
        )
    }
}

impl Handler<SetWindowSize> for ApplicationService {
    type Result = ();

    fn handle(&mut self, SetWindowSize(w, h): SetWindowSize, _: &mut Self::Context) {
        self.window_width = w as u32;
        self.window_height = h as u32;
    }
}

impl Handler<SearchForDevice> for ApplicationService {
    type Result = ResponseActFuture<Self, ()>;

    fn handle(&mut self, _: SearchForDevice, ctx: &mut Self::Context) -> Self::Result {
        let notification_service = self.notification_service.clone();
        let self_addr = ctx.address();
        notification_service.do_send(DisplayNotification(
            "Device search".to_string(),
            "Scanning for device".to_string(),
            NotificationType::Info,
        ));
        let addr = self.upload_options.as_ref().map(|o| o.addr);
        Box::pin(
            async move {
                if let Some(addr) = addr {
                    if let Ok((addr, mac)) =
                        with_retry(3, Duration::from_millis(500), || ping_ip(addr)).await
                    {
                        notification_service.do_send(DisplayNotification(
                            "Device search".to_string(),
                            format!("Found device {mac:x?} at {addr}"),
                            NotificationType::Success,
                        ));
                        tokio::time::sleep(Duration::from_millis(200)).await;
                        let ip_addr = addr.ip();
                        self_addr.do_send(Connect(Some(ip_addr)));
                        return;
                    }
                }
                let ip = if let Ok(IpAddr::V4(ip)) = local_ip_address::local_ip() {
                    println!("{ip}");
                    ip
                } else {
                    notification_service.do_send(DisplayNotification(
                        "Device search".to_string(),
                        "Unable to obtain local ip address".to_string(),
                        NotificationType::Error,
                    ));
                    return;
                };
                let bytes = ip.octets();
                let futures = (0..=255).map(|x| {
                    let notification_service = notification_service.clone();
                    let ip = [bytes[0], bytes[1], bytes[2], x];
                    Box::pin(async move {
                        let (addr, mac) = ping_ip(ip).await?;
                        notification_service.do_send(DisplayNotification(
                            "Device search".to_string(),
                            format!("Found device {mac:x?} at {addr}"),
                            NotificationType::Success,
                        ));
                        Ok::<_, anyhow::Error>(addr)
                    })
                });
                let res = futures::future::select_ok(futures)
                    .await
                    .ok_or_notify(&notification_service);
                let (socket_addr, _) = match res {
                    Some(a) => a,
                    None => return,
                };
                tokio::time::sleep(Duration::from_millis(200)).await;
                let ip_addr = socket_addr.ip();
                self_addr.do_send(Connect(Some(ip_addr)));
            }
            .into_actor(self),
        )
    }
}

pub fn draw_selection_box(
    from: Point,
    to: Point,
    transform: Transform,
    stroke: &Stroke,
    mut buf: SharedPixelBuffer<Rgba8Pixel>,
) -> SharedPixelBuffer<Rgba8Pixel> {
    let width = buf.width();
    let height = buf.height();
    let mut pixmap = tiny_skia::PixmapMut::from_bytes(buf.make_mut_bytes(), width, height).unwrap();
    let rect = Rect::from_ltrb(
        from.x.min(to.x),
        from.y.min(to.y),
        from.x.max(to.x),
        from.y.max(to.y),
    )
    .unwrap();
    let path = tiny_skia::PathBuilder::from_rect(rect);
    pixmap.stroke_path(
        &path,
        &Paint {
            anti_alias: false,
            shader: tiny_skia::Shader::SolidColor(tiny_skia::Color::from_rgba8(0, 0, 0, 128)),
            ..Default::default()
        },
        stroke,
        transform,
        None,
    );
    buf
}

pub fn draw_selected_figures<'a, T: IntoIterator<Item = &'a Figure>>(
    selected_figures: T,
    transform: Transform,
    width: u32,
    height: u32,
    scale: f32,
) -> SharedPixelBuffer<Rgba8Pixel>
where
    <T as IntoIterator>::IntoIter: Clone,
{
    let mut pixel_buffer = SharedPixelBuffer::<Rgba8Pixel>::new(width, height);
    let mut pixmap =
        tiny_skia::PixmapMut::from_bytes(pixel_buffer.make_mut_bytes(), width, height).unwrap();
    let bounds = selected_figures.into_iter().map(|f| f.bounds());
    for bounds in bounds.clone() {
        let rect = Rect::from_ltrb(
            bounds.left() - 5. / scale,
            bounds.top() - 5. / scale,
            bounds.right() + 5. / scale,
            bounds.bottom() + 5. / scale,
        )
        .unwrap();
        let path = tiny_skia::PathBuilder::from_rect(rect);
        pixmap.stroke_path(
            &path,
            &Paint {
                anti_alias: false,
                shader: tiny_skia::Shader::SolidColor(tiny_skia::Color::from_rgba8(0, 0, 0, 128)),
                ..Default::default()
            },
            &tiny_skia::Stroke {
                width: 1. / scale,
                dash: tiny_skia::StrokeDash::new(vec![5.0 / scale, 5.0 / scale], 0.0),
                ..Default::default()
            },
            transform,
            None,
        );
    }
    let cmp = |a: &f32, b: &f32| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal);
    let left = bounds.clone().map(|b| b.left()).min_by(cmp);
    let right = bounds.clone().map(|b| b.right()).max_by(cmp);
    let top = bounds.clone().map(|b| b.top()).min_by(cmp);
    let bottom = bounds.clone().map(|b| b.bottom()).max_by(cmp);
    if let Some(((left, right), (top, bottom))) = left.zip(right).zip(top.zip(bottom)) {
        let rect = Rect::from_ltrb(left, top, right, bottom).unwrap();
        let path = tiny_skia::PathBuilder::from_rect(rect);
        pixmap.stroke_path(
            &path,
            &Paint {
                anti_alias: false,
                shader: tiny_skia::Shader::SolidColor(tiny_skia::Color::from_rgba8(0, 0, 255, 128)),
                ..Default::default()
            },
            &tiny_skia::Stroke {
                width: 1. / scale,
                dash: tiny_skia::StrokeDash::new(vec![5.0 / scale, 5.0 / scale], 0.0),
                ..Default::default()
            },
            transform,
            None,
        );
        let offset = FIGURE_CONTROL_WIDTH / scale;
        let mut fill_rect = |rect| {
            let path = tiny_skia::PathBuilder::from_rect(rect);
            pixmap.fill_path(
                &path,
                &Paint {
                    anti_alias: false,
                    shader: tiny_skia::Shader::SolidColor(tiny_skia::Color::from_rgba8(
                        0, 0, 255, 128,
                    )),
                    ..Default::default()
                },
                Default::default(),
                transform,
                None,
            );
        };
        fill_rect(
            Rect::from_ltrb(left - offset, top - offset, left + offset, top + offset).unwrap(),
        );
        fill_rect(
            Rect::from_ltrb(right - offset, top - offset, right + offset, top + offset).unwrap(),
        );
        fill_rect(
            Rect::from_ltrb(
                left - offset,
                bottom - offset,
                left + offset,
                bottom + offset,
            )
            .unwrap(),
        );
        fill_rect(
            Rect::from_ltrb(
                right - offset,
                bottom - offset,
                right + offset,
                bottom + offset,
            )
            .unwrap(),
        );
    }
    pixel_buffer
}

pub fn sort_by_smallest(mut v: Vec<Path>) -> Vec<Path> {
    v.sort_by(|a, b| {
        path_area(a)
            .partial_cmp(&path_area(b))
            .unwrap_or(std::cmp::Ordering::Equal)
    });
    v
}

pub fn contours_from_path(p: &Path) -> Vec<Path> {
    let mut last_path = PathBuilder::new();
    let mut res = Vec::new();
    for segment in p.segments() {
        match segment {
            PathSegment::MoveTo(Point { x, y }) => {
                let mut new_path = PathBuilder::new();
                std::mem::swap(&mut new_path, &mut last_path);
                if let Some(path) = new_path.finish() {
                    res.push(path);
                }
                last_path.move_to(x, y);
            }
            PathSegment::LineTo(Point { x, y }) => {
                last_path.line_to(x, y);
            }
            PathSegment::QuadTo(a, b) => {
                last_path.quad_to(a.x, a.y, b.x, b.y);
            }
            PathSegment::CubicTo(a, b, c) => {
                last_path.cubic_to(a.x, a.y, b.x, b.y, c.x, c.y);
            }
            PathSegment::Close => {
                last_path.close();
            }
        }
    }
    if let Some(path) = last_path.finish() {
        res.push(path);
    }
    res
}

pub fn points_from_path(p: &Path) -> Vec<(f32, f32, bool)> {
    let mut initial = None;
    let mut res = vec![];
    let mut pos = (0., 0.);
    for segment in p.segments() {
        match segment {
            PathSegment::MoveTo(Point { x, y }) => {
                initial = Some((x, y));
                pos.0 = x;
                pos.1 = y;
                res.push((x, y, false));
            }
            PathSegment::LineTo(Point { x, y }) => {
                initial = initial.or(Some((pos.0, pos.1)));
                res.push((x, y, true));
                pos.0 = x;
                pos.1 = y;
            }
            PathSegment::QuadTo(a, b) => {
                let mut r = interpolate_bezier_quad((pos.into(), a, b), BEZIER_STEPS)
                    .into_iter()
                    .map(|Point { x, y }| (x, y, true))
                    .collect();
                res.append(&mut r);
                pos.0 = b.x;
                pos.1 = b.y;
            }
            PathSegment::CubicTo(a, b, c) => {
                let mut r = interpolate_bezier_cubic((pos.into(), a, b, c), BEZIER_STEPS)
                    .into_iter()
                    .map(|Point { x, y }| (x, y, true))
                    .collect();
                res.append(&mut r);
                pos.0 = c.x;
                pos.1 = c.y;
            }
            PathSegment::Close => {
                if let Some((x, y)) = initial {
                    res.push((x, y, true));
                }
            }
        }
    }
    res
}

pub fn draw_rulers_vertical(
    buffer: &mut SharedPixelBuffer<Rgba8Pixel>,
    height: u32,
    scale: f32,
    ppmm: f32,
    cache: &mut piet_tiny_skia::Cache,
    y: f32,
    viewport_height: u32,
) -> Result<(), anyhow::Error> {
    let mut pixmap = tiny_skia::PixmapMut::from_bytes(
        buffer.make_mut_bytes(),
        RULER_WIDTH,
        height + RULER_WIDTH,
    )
    .context("Unable to create pixmap")?;

    let mut rc = cache.render_context(&mut pixmap);

    let y = y / ppmm;
    let max = viewport_height as f32 / scale / ppmm;
    let mut step = 1;
    while max / step as f32 > 10. {
        step *= 5;
    }
    if step >= 5 && (max / step as f32) < 2.5 {
        step /= 5;
    }
    let max = max + y;

    let mut rulers = tiny_skia::PathBuilder::new();
    for i in (y as u32 / step)..=(max as u32 / step) {
        let num = i * step;
        let text = rc
            .text()
            .new_text_layout(num.to_string())
            .font(piet::FontFamily::MONOSPACE, 10.)
            .build()
            .map_err(|err| anyhow::anyhow!("{err:?}"))?;
        for i in ((i * 5)..((i + 1) * 5)).skip(1) {
            let num = (i as f32 / 5.) * step as f32;
            let y = RULER_WIDTH as f32 + (height as f32 * (num / (max - y)) - (y * ppmm * scale));
            if y >= RULER_WIDTH as f32 {
                rulers.move_to(0., y);
                rulers.line_to(5., y);
            }
        }
        let y =
            RULER_WIDTH as f32 + (height as f32 * (num as f32 / (max - y)) - (y * ppmm * scale));
        if y >= RULER_WIDTH as f32 {
            {
                let y = y as f64 - text.size().height / 2.;
                rc.draw_text(&text, piet::kurbo::Point { x: 10., y });
            }
            rulers.move_to(0., y);
            rulers.line_to(8., y);
        }
    }
    if let Some(rulers) = rulers.finish() {
        pixmap.stroke_path(
            &rulers,
            &tiny_skia::Paint::default(),
            &tiny_skia::Stroke::default(),
            Default::default(),
            None,
        );
    }
    Ok(())
}

pub fn draw_rulers_horizontal(
    buffer: &mut SharedPixelBuffer<Rgba8Pixel>,
    width: u32,
    scale: f32,
    ppmm: f32,
    cache: &mut piet_tiny_skia::Cache,
    x: f32,
    viewport_width: u32,
) -> Result<(), anyhow::Error> {
    let mut pixmap =
        tiny_skia::PixmapMut::from_bytes(buffer.make_mut_bytes(), width + RULER_WIDTH, RULER_WIDTH)
            .context("Unable to create pixmap")?;
    let mut rc = cache.render_context(&mut pixmap);

    let x = x / ppmm;
    let max = viewport_width as f32 / scale / ppmm;
    let mut step = 1;
    while max / step as f32 > 10. {
        step *= 5;
    }
    if step >= 5 && (max / step as f32) < 4. {
        step /= 5;
    }
    let max = max + x;
    let mut rulers = tiny_skia::PathBuilder::new();
    for i in (x as u32 / step)..=(max as u32 / step) {
        let num = i * step;
        let text = rc
            .text()
            .new_text_layout(num.to_string())
            .font(piet::FontFamily::MONOSPACE, 10.)
            .build()
            .map_err(|err| anyhow::anyhow!("{err:?}"))?;
        for i in ((i * 5)..((i + 1) * 5)).skip(1) {
            let num = (i as f32 / 5.) * step as f32;
            let x = RULER_WIDTH as f32 + (width as f32 * (num / (max - x)) - (x * ppmm * scale));
            if x >= RULER_WIDTH as f32 {
                rulers.move_to(x, 0.);
                rulers.line_to(x, 5.);
            }
        }
        let x = RULER_WIDTH as f32 + (width as f32 * (num as f32 / (max - x)) - (x * ppmm * scale));
        if x >= RULER_WIDTH as f32 {
            {
                let x = x as f64 - text.size().width / 2.;
                rc.draw_text(&text, piet::kurbo::Point { x, y: 10. });
            }
            rulers.move_to(x, 0.);
            rulers.line_to(x, 8.);
        }
    }
    if let Some(rulers) = rulers.finish() {
        pixmap.stroke_path(
            &rulers,
            &tiny_skia::Paint::default(),
            &tiny_skia::Stroke::default(),
            Default::default(),
            None,
        );
    }
    Ok(())
}

pub fn draw_device_position(
    buffer: &mut SharedPixelBuffer<Rgba8Pixel>,
    device_status: &DeviceStatus,
    trace: &[(f32, f32)],
    transform: Transform,
) {
    let width = buffer.width();
    let height = buffer.height();
    let mut pixmap =
        tiny_skia::PixmapMut::from_bytes(buffer.make_mut_bytes(), width, height).unwrap();
    let mut path = tiny_skia::PathBuilder::new();
    let circle_radius = 10. / transform.sx;
    let x = device_status.x;
    let y = device_status.y;

    if let Some((x, y)) = trace.first().copied() {
        path.move_to(x, y);
    }
    for (x, y) in trace.iter().skip(1).copied() {
        path.line_to(x, y);
    }

    path.push_circle(x, y, circle_radius);

    path.move_to(x, y - circle_radius);
    path.line_to(x, y - circle_radius * 1.5);
    path.move_to(x, y + circle_radius);
    path.line_to(x, y + circle_radius * 1.5);

    path.move_to(x - circle_radius, y);
    path.line_to(x - circle_radius * 1.5, y);
    path.move_to(x + circle_radius, y);
    path.line_to(x + circle_radius * 1.5, y);

    path.push_circle(x, y, 1. / transform.sx);

    let stroke = tiny_skia::Stroke {
        width: 2. / transform.sx,
        ..Default::default()
    };
    let path = path.finish().unwrap();
    pixmap.stroke_path(
        &path,
        &Paint {
            shader: tiny_skia::Shader::SolidColor(tiny_skia::Color::from_rgba8(0, 128, 0, 128)),
            ..Default::default()
        },
        &stroke,
        transform,
        None,
    );
}

fn weighted_marker(a: Point, b: Point, weight: f32) -> Point {
    (a.x - (a.x - b.x) * weight, a.y - (a.y - b.y) * weight).into()
}

fn interpolate_bezier_quad((a, b, c): (Point, Point, Point), steps: usize) -> Vec<Point> {
    let mut res = vec![];
    let step = 1. / steps as f32;
    for x in 1..=steps {
        let ratio = step * x as f32;
        let ab = weighted_marker(a, b, ratio);
        let bc = weighted_marker(b, c, ratio);
        res.push(weighted_marker(ab, bc, ratio));
    }
    res
}

fn interpolate_bezier_cubic(
    (a, b, c, d): (Point, Point, Point, Point),
    steps: usize,
) -> Vec<Point> {
    let mut res = vec![];
    let step = 1. / steps as f32;
    for x in 1..=steps {
        let ratio = step * x as f32;
        let ab = weighted_marker(a, b, ratio);
        let bc = weighted_marker(b, c, ratio);
        let cd = weighted_marker(c, d, ratio);
        let abc = weighted_marker(ab, bc, ratio);
        let bcd = weighted_marker(bc, cd, ratio);
        let abcd = weighted_marker(abc, bcd, ratio);
        res.push(abcd);
    }
    res
}

fn bound_move(
    bounds: Rect,
    (mut delta_x, mut delta_y): (f32, f32),
    width: u32,
    height: u32,
    ppmm: f32,
) -> (f32, f32) {
    if bounds.left() + delta_x <= 0. {
        delta_x = -bounds.left();
    }
    if bounds.right() + delta_x >= width as f32 * ppmm {
        delta_x = width as f32 * ppmm - bounds.right();
    }
    if bounds.top() + delta_y <= 0. {
        delta_y = -bounds.top();
    }
    if bounds.bottom() + delta_y >= height as f32 * ppmm {
        delta_y = height as f32 * ppmm - bounds.bottom();
    }
    (delta_x, delta_y)
}

fn cut_time<'a, T: IntoIterator<Item = &'a F>, F: 'a + std::borrow::Borrow<Figure>>(
    figures: T,
    cut_rate: CutRate,
    ppmm: f32,
) -> f32 {
    let mut last_pos = (0., 0.);
    let mut res = 0.;
    for f in figures {
        let mut pos = (0., 0.);
        let mut initial = None;
        for segment in f.borrow().path.segments() {
            let points = match segment {
                PathSegment::MoveTo(Point { x, y }) => {
                    initial = Some((x, y));
                    pos.0 = x;
                    pos.1 = y;
                    vec![(x, y)]
                }
                PathSegment::LineTo(Point { x, y }) => {
                    initial = initial.or(Some((pos.0, pos.1)));
                    pos.0 = x;
                    pos.1 = y;
                    vec![(x, y)]
                }
                PathSegment::QuadTo(a, b) => {
                    let res = interpolate_bezier_quad((pos.into(), a, b), 5);
                    pos.0 = b.x;
                    pos.1 = b.y;
                    res.into_iter().map(|Point { x, y }| (x, y)).collect()
                }
                PathSegment::CubicTo(a, b, c) => {
                    let res = interpolate_bezier_cubic((pos.into(), a, b, c), 5);
                    pos.0 = c.x;
                    pos.1 = c.y;
                    res.into_iter().map(|Point { x, y }| (x, y)).collect()
                }
                PathSegment::Close => {
                    if let Some((x, y)) = initial {
                        pos.0 = x;
                        pos.1 = y;
                        vec![(x, y)]
                    } else {
                        vec![]
                    }
                }
            };
            let mut len;
            for (x, y) in points {
                len =
                    ((x - last_pos.0).abs().powi(2) + (y - last_pos.1).abs().powi(2)).sqrt() / ppmm;
                last_pos = (x, y);
                res += 1. / (cut_rate / len) * 1000.;
            }
        }
    }
    res
}

fn write_gcode<W: Write, F: IntoIterator<Item = Arc<Figure>>>(
    figures: F,
    w: &mut W,
) -> Result<(), std::io::Error> {
    for f in figures.into_iter() {
        for (x, y, z) in points_from_path(&f.path) {
            let x = format!("{}", x as u32);
            let y = format!("{}", y as u32);
            let buf = match z {
                true => format!("G1 X{x:0>10} Y{y:0>10}\n"),
                false => format!("G0 X{x:0>10} Y{y:0>10}\n"),
            };
            w.write_all(buf.as_bytes()).unwrap();
        }
    }
    Ok(())
}

async fn write_gcode_async<
    W: AsyncWriteExt + std::marker::Unpin,
    F: IntoIterator<Item = Arc<Figure>>,
>(
    figures: F,
    w: &mut W,
) -> Result<(), std::io::Error> {
    let mut hasher = Sha256::new();
    let mut hash = [0; 8];
    for path in figures
        .into_iter()
        .map(|f| f.path.clone())
        .map(|p| contours_from_path(&p))
        .flat_map(sort_by_smallest)
    {
        for (x, y, z) in points_from_path(&path) {
            let x = (x as u32).to_be_bytes();
            let y = (y as u32).to_be_bytes();
            let buf = [z as u8, x[0], x[1], x[2], x[3], y[0], y[1], y[2], y[3]];
            hasher.update([&buf[..], &hash[..]].concat());
            w.write_all(&buf).await?;
            let res = &hasher.finalize_reset();
            w.write_all(&res[..8]).await?;
            hash = res[..8].try_into().unwrap();
        }
    }
    Ok(())
}

#[atomic_enum]
#[derive(PartialEq)]
pub enum Mode {
    Select,
    Line,
}

impl Default for Mode {
    fn default() -> Self {
        Self::Select
    }
}

async fn send_command(addr: SocketAddr, command: TcpCommand) -> Result<(), anyhow::Error> {
    let mut stream = TcpStream::connect(&addr).await?;
    stream.set_nodelay(true)?;
    let msg = format!(
        "{:512}",
        serde_json::to_string(&TcpQuery { command }).unwrap()
    );
    stream.write_all(msg.as_bytes()).await?;
    stream.flush().await?;
    Ok(())
}

async fn with_timeout<RF: Future<Output = T> + std::marker::Unpin, T>(
    timeout: Duration,
    f: RF,
) -> Option<T> {
    let sleep = std::pin::pin!(tokio::time::sleep(timeout));
    let res = futures::future::select(f, sleep).await;
    match res {
        Either::Left((t, _)) => Some(t),
        Either::Right(_) => None,
    }
}

async fn with_retry<F: Fn() -> RF, RF: Future<Output = Result<T, ERR>>, T, ERR>(
    n: usize,
    delay: Duration,
    f: F,
) -> Result<T, ERR> {
    let mut c = 0;
    loop {
        match f().await {
            Ok(r) => return Ok(r),
            Err(_) if c < n => {
                c += 1;
                tokio::time::sleep(delay).await;
                continue;
            }
            Err(err) => return Err(err),
        };
    }
}

async fn ping_ip<I: Into<IpAddr>>(ip: I) -> Result<(SocketAddr, Vec<u8>), anyhow::Error> {
    let socket_addr = std::net::SocketAddr::from((ip.into(), 8080));
    let fut = std::pin::pin!(TcpStream::connect(&socket_addr));
    let mut stream = with_timeout(Duration::from_millis(5000), fut)
        .await
        .ok_or(anyhow::anyhow!("Timeout"))??;
    let addr = stream.peer_addr()?;
    stream.set_nodelay(true)?;
    let msg = format!(
        "{:512}",
        serde_json::to_string(&TcpQuery {
            command: TcpCommand::Ping,
        })
        .unwrap()
    );
    stream.write_all(msg.as_bytes()).await?;
    stream.flush().await.unwrap();
    let mut mac = Vec::new();
    stream.read_to_end(&mut mac).await?;
    Ok::<_, anyhow::Error>((addr, mac))
}

#[cfg(test)]
mod test {
    use super::*;
    use tiny_skia::{PathBuilder, Rect};

    #[test]
    fn sorts_by_optimal_path() {
        let figure = |l, t, r, b| {
            Arc::new(Figure::new(PathBuilder::from_rect(
                Rect::from_ltrb(l, t, r, b).unwrap(),
            )))
        };
        let fig1 = figure(30., 0., 40., 10.);
        let fig2 = figure(10., 0., 20., 10.);
        let fig3 = figure(60., 0., 80., 10.);
        let input = vec![fig1.clone(), fig2.clone(), fig3.clone()];

        let expected = vec![fig2, fig1, fig3];

        assert_eq!(sort_by_optimal_path(input, (0., 0.).into()), expected);
    }
}
